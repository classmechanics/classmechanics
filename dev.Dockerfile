FROM python:3.5

ENV PYTHONUNBUFFERED 1

COPY compose/django/nvm-install.sh /nvm-install.sh
RUN /nvm-install.sh
RUN bash -c "source ~/.nvm/nvm.sh && nvm install v5.4.1"
RUN bash -c "source ~/.nvm/nvm.sh && nvm use v5.4.1 && npm install -g less"

# Requirements have to be pulled and installed here, otherwise caching won't work
RUN pip install -U pip
RUN mkdir /requirements
COPY requirements/base.txt /requirements
RUN pip install -r /requirements/base.txt
COPY requirements/development.txt /requirements
RUN pip install -r /requirements/development.txt
COPY requirements/testing.txt /requirements
RUN pip install -r /requirements/testing.txt

COPY compose/django/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]
