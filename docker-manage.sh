#!/usr/bin/env bash
#
# Wrapper around running ./manage.py from within a django docker container.

cd $(dirname $0)

docker-compose run django python manage.py $@
