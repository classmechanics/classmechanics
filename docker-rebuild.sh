#!/usr/bin/env bash
#
# Rebuilds the django container image, then restarts it and tails the logs.

docker-compose build django && \
./docker-restart.sh
