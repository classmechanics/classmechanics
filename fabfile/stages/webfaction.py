try:
    import fabric
except ImportError:
    pass
else:

    from collections import namedtuple

    from fabric.api import env, task

    __all__ = [
        'WFPROD_SETTINGS',
        'WFSTAGING_SETTINGS',
        'wfprod',
        'wfstaging',
    ]


    Repo = namedtuple('Repo', 'name url ref')
    PROD_REPOS = [
        Repo('classmechanics', 'git@gitlab.com:classmechanics/classmechanics', 'production'),
    ]
    STAGING_REPOS = [
        Repo('classmechanics', 'git@gitlab.com:classmechanics/classmechanics', 'master'),
    ]


    WFPROD_SETTINGS = dict(
        settings = 'webfaction-production',
        host_string = 'ostpvt.webfactional.com',
        forward_agent = True,
        user = 'ostpvt',
        venv_name = 'cm-prod',
        venv_path = None,
        webapp_name = 'cm_prod_django',
        repos = PROD_REPOS,
        primary_repo_name = 'classmechanics',
        requirements = 'requirements/production.txt',
        rollbar_environment = 'production',
        local_repo_path = '.',
    )
    WFPROD_SETTINGS.update(dict(
        webapp_path = '$HOME/webapps/{webapp_name}'.format(**WFPROD_SETTINGS),
        wsgi_script = '~/proj/{venv_name}/{primary_repo_name}/classmechanics/wsgi.py'.format(**WFPROD_SETTINGS),
    ))

    WFSTAGING_SETTINGS = dict(
        settings = 'webfaction-staging',
        host_string = 'ostpvt.webfactional.com',
        forward_agent = True,
        user = 'ostpvt',
        venv_name = 'cm-staging',
        venv_path = None,
        webapp_name = 'cm_staging_django',
        repos = STAGING_REPOS,
        primary_repo_name = 'classmechanics',
        requirements = 'requirements/production.txt',
        rollbar_environment = 'staging',
        local_repo_path='.',
    )
    WFSTAGING_SETTINGS.update(dict(
        webapp_path = '$HOME/webapps/{webapp_name}'.format(**WFSTAGING_SETTINGS),
        wsgi_script = '~/proj/{venv_name}/{primary_repo_name}/classmechanics/wsgi.py'.format(**WFSTAGING_SETTINGS),
    ))


    @task
    def wfprod():
        env.update(WFPROD_SETTINGS)


    @task
    def wfstaging():
        env.update(WFSTAGING_SETTINGS)
