from __future__ import print_function

try:
    import fabric
except ImportError:
    pass
else:
    import os
    import sys

    # noinspection PyUnresolvedReferences
    from fabric.api import env, get, local, prefix, put, require, run, settings, task
    # noinspection PyUnresolvedReferences
    from fabric.contrib.files import exists
    # noinspection PyUnresolvedReferences
    import requests

    from .stages.webfaction import *


    sys.path.append(os.path.abspath(os.path.dirname(__file__)))


    @task
    def setup_env():
        """
        Run this one time to set up the environment on WebFaction.
        """
        require('settings')
        make_env()
        clone_repo()
        pull_repo()
        checkout_branch()
        install_deps()
        put('fabfile/files/postactivate', '~/.virtualenvs/{venv_name}/bin/postactivate'.format(**env), mode=0o700)
        if not exists('~/.virtualenvs/{venv_name}/bin/environment.py'.format(**env)):
            put('fabfile/files/environment.py', '~/.virtualenvs/{venv_name}/bin/environment.py'.format(**env), mode=0o700)


    @task
    def setup_apache():
        put('fabfile/files/httpd-{settings}.conf'.format(**env), '~/webapps/{webapp_name}/apache2/conf/httpd.conf'.format(**env), mode=0o700)


    @task
    def deploy():
        require('settings')
        setup_env()
        setup_apache()
        manage('migrate --noinput')
        collectstatic()
        manage('compress')
        # manage('waffle_update')
        apache('restart')
        rollbar_record_deploy(env.rollbar_environment)


    @task
    def collectstatic():
        manage('collectstatic --noinput')


    @task
    def make_env():
        require('settings')
        run('( [ -d $HOME/.virtualenvs/{venv_name} ] && [ -d $HOME/proj/{venv_name} ] ) || mkproject -p /usr/local/bin/python2.7 {venv_name}'.format(**env))


    @task
    def clone_repo():
        require('settings')
        with _workon():
            for repo in env.repos:
                run('[ -d {0.name} ] || git clone {0.url} {0.name}'.format(repo))


    @task
    def pull_repo():
        require('settings')
        for repo in env.repos:
            with _workon(cd=repo.name):
                run('git pull --all')


    @task
    def checkout_branch():
        require('settings')
        for repo in env.repos:
            with _workon(cd=repo.name):
                run('git checkout {0.ref}'.format(repo))


    @task
    def install_deps():
        require('settings')
        for repo in env.repos:
            with _workon(cd=repo.name):
                run('pwd; pip install -r {requirements}'.format(**env))
                run('python setup.py develop')


    @task
    def apache(action):
        require('settings')
        run('{webapp_path}/apache2/bin/{0}'.format(action, **env))


    @task
    def touch_wsgi():
        require('settings')
        run('touch {wsgi_script}'.format(**env))


    @task
    def manage(command, vars=''):
        require('settings')
        with _workon(cd=env.primary_repo_name):
            run('{vars} django-admin.py {command}'.format(**locals()))


    ##############################################################################
    # CONTEXT MANAGERS

    def _workon(cd='.'):
        if env.venv_name:
            return prefix('workon {venv_name} && cd {0}'.format(cd, **env))
        elif env.venv_path:
            return prefix('source {venv_path}/bin/activate && cd {0}'.format(cd, **env))
        else:
            raise KeyError('Neither venv_name nor venv_path were found in env.')


    ##############################################################################
    # ROLLBAR

    def rollbar_record_deploy(environment):
        return  # TODO: create rollbar org for classmechanics

        # noinspection PyUnreachableCode
        access_token = os.getenv('ROLLBAR_SERVER_ACCESS_TOKEN')
        local_username = local('whoami', capture=True)
        # fetch last committed revision in the locally-checked out branch
        revision = local('cd {}; git log -n 1 --pretty=format:"%H"'.format(env.local_repo_path), capture=True)

        resp = requests.post('https://api.rollbar.com/api/1/deploy/', {
            'access_token': access_token,
            'environment': environment,
            'local_username': local_username,
            'revision': revision
        }, timeout=3)

        if resp.status_code == 200:
            print("Deploy recorded successfully.")
        else:
            print("Error recording deploy:", resp.text)


    ##############################################################################
    # DB COPYING

    @task
    def copy_prod_to_dev():
        PROD_PATH = 'proj/cm-prod/prod.sql.gz'
        LOCAL_PATH = '/tmp/cm-prod.sql.gz'
        with settings(**WFPROD_SETTINGS):
            run('$HOME/proj/cm-prod/dump-prod-sql.sh')
            get(PROD_PATH, LOCAL_PATH)
        local('dropdb -h docker.dev -p 9632 -U postgres postgres || true')
        local('createdb -h docker.dev -p 9632 -U postgres --owner=postgres --template=template0 --encoding=utf8 postgres')
        local('django-admin migrate --noinput')
        local('gunzip < {} | psql --quiet -h docker.dev -p 9632 -U postgres --single-transaction postgres'.format(LOCAL_PATH))
        os.unlink(LOCAL_PATH)
