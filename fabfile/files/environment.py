from os import environ


SETTINGS = dict(
    DJANGO_SETTINGS_MODULE='classmechanics.settings.production',
    DATABASE_URL="",
    EMAIL_HOST="",
    EMAIL_HOST_USER="",
    EMAIL_HOST_PASSWORD="",
    EMAIL_PORT="",
    SERVER_EMAIL="",
    DEFAULT_FROM_EMAIL="",
    SECRET_KEY="",
    ALLOWED_HOSTS="",
    ADMIN_EMAILS="",
    ROLLBAR_SERVER_ACCESS_TOKEN="",
    ROLLBAR_CLIENT_ACCESS_TOKEN="",
    PIWIK_HOST="piwik.11craft.com",
    PIWIK_SITE_ID="",
)

environ.update(SETTINGS)


if __name__ == '__main__':
    for key, value in SETTINGS.items():
        print('export {key}={value!r}'.format(**locals()))
