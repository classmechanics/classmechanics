import datetime

from django.conf import settings
from django.contrib.sites.models import Site


def analytics(request):
    return {
        'PIWIK_HOST': settings.PIWIK_HOST,
        'PIWIK_SITE_ID': settings.PIWIK_SITE_ID,
    }


def current_date(request):
    return {
        'current_date': datetime.date.today(),
    }


def debug(request):
    return {
        'DEBUG': settings.DEBUG,
    }


def default_http_protocol(request):
    return {
        'default_http_protocol': settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL
    }


def site(request):
    return {
        'site': Site.objects.get_current(),
    }
