from .development import *

INSTALLED_APPS += [
    'django_behave',
]

# cf. http://stackoverflow.com/a/15890638/72560
TEST_MODE = False   # Set to True before running tests.
TEST_RUNNER = 'classmechanics.testrunner.TestModeRunner'

from .ssl import inject_sslify_middleware
MIDDLEWARE_CLASSES = inject_sslify_middleware(MIDDLEWARE_CLASSES)

SESSION_COOKIE_SECURE = True
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'

# http://model-mommy.readthedocs.org/en/latest/how_mommy_behaves.html#custom-fields
def gen_markupfield():
    return '# Heading\n\nBody\n\n1. One\n2. Two\n3. Three'
MOMMY_CUSTOM_FIELDS_GEN = {
    'markupfield.fields.MarkupField': gen_markupfield,
}
