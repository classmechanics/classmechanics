"""
Project main settings file. These settings are common to the project
if you need to override something do it in local.pt
"""

import os
from sys import path

import dj_database_url
import environ

env = environ.Env()


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

## PATHS
# Path containing the django project
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path.append(BASE_DIR)

# Path of the top level directory.
# This directory contains the django project, apps, libs, etc...
PROJECT_ROOT = os.path.dirname(BASE_DIR)

# Add apps and libs to the PROJECT_ROOT
path.append(os.path.join(PROJECT_ROOT, "apps"))
path.append(os.path.join(PROJECT_ROOT, "libs"))


## SITE SETTINGS
# https://docs.djangoproject.com/en/1.8/ref/settings/#site-id
SITE_ID = 1

# https://docs.djangoproject.com/en/1.8/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# https://docs.djangoproject.com/en/1.8/ref/settings/#installed-apps
INSTALLED_APPS = [
    'flat',  # django-flat-theme must be BEFORE contrib.admin

    # Django apps
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'django.contrib.syndication',
    'django.contrib.staticfiles',

    # Third party apps
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.stripe',
    'cloudinary',
    'colorfield',
    'compressor',
    'django_extensions',
    'django_forms_bootstrap',
    'django_fsm_log',
    'djmoney',
    'fsm_admin',
    'subdomains',

    # Local apps
    'classmechanics',
    'classmechanics.apps.accounts',
    'classmechanics.apps.analytics',
    'classmechanics.apps.brands',
    'classmechanics.apps.camps',
    'classmechanics.apps.classes',
    'classmechanics.apps.coupons',
    'classmechanics.apps.emails',
    'classmechanics.apps.families',
    'classmechanics.apps.forums',
    'classmechanics.apps.instructors',
    'classmechanics.apps.locations',
    'classmechanics.apps.orders',
    'classmechanics.apps.organizations',
    'classmechanics.apps.pages',
    'classmechanics.apps.punchcards',
    'classmechanics.apps.registrations',
    'classmechanics.apps.stripepayments',
]

# https://docs.djangoproject.com/en/1.8/topics/auth/passwords/#using-bcrypt-with-django
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

## DEBUG SETTINGS
# https://docs.djangoproject.com/en/1.8/ref/settings/#debug
DEBUG = False

# https://docs.djangoproject.com/en/1.8/ref/settings/#internal-ips
INTERNAL_IPS = ('127.0.0.1',)

## LOCALE SETTINGS
# Local time zone for this installation.
# https://docs.djangoproject.com/en/1.8/ref/settings/#time-zone
TIME_ZONE = 'America/Los_Angeles'

# https://docs.djangoproject.com/en/1.8/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# https://docs.djangoproject.com/en/1.8/ref/settings/#use-i18n
USE_I18N = True

# https://docs.djangoproject.com/en/1.8/ref/settings/#use-l10n
USE_L10N = True

# https://docs.djangoproject.com/en/1.8/ref/settings/#use-tz
USE_TZ = True


## MEDIA AND STATIC SETTINGS
# Absolute filesystem path to the directory that will hold user-uploaded files.
# https://docs.djangoproject.com/en/1.8/ref/settings/#media-root
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'public/media')

# URL that handles the media served from MEDIA_ROOT. Use a trailing slash.
# https://docs.djangoproject.com/en/1.8/ref/settings/#media-url
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# https://docs.djangoproject.com/en/1.8/ref/settings/#static-root
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'public/static')

# URL prefix for static files.
# https://docs.djangoproject.com/en/1.8/ref/settings/#static-url
STATIC_URL = '/static/'

# Additional locations of static files
# https://docs.djangoproject.com/en/1.8/ref/settings/#staticfiles-dirs
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# https://docs.djangoproject.com/en/1.8/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

## TEMPLATE SETTINGS
# https://docs.djangoproject.com/en/1.8/ref/settings/#templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.debug',
                'django.core.context_processors.media',
                'django.core.context_processors.request',
                'django.core.context_processors.i18n',
                'django.core.context_processors.static',
                'django.core.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'classmechanics.context_processors.analytics',
                'classmechanics.context_processors.current_date',
                'classmechanics.context_processors.debug',
                'classmechanics.context_processors.default_http_protocol',
                'classmechanics.context_processors.site',
            ],
        },
    },
]


## MIDDLEWARE SETTINGS
# See: https://docs.djangoproject.com/en/1.8/ref/settings/#middleware-classes
MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'classmechanics.apps.brands.middleware.BrandMiddleware',
    'django.middleware.common.CommonMiddleware',
    'classmechanics.middleware.SubdomainAwareCsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'classmechanics.apps.families.middleware.FamilyMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
]


## DATABASE SETTINGS
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = dict(default=dj_database_url.config())


## DJANGO-COMPRESSOR SETTINGS
COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
)
STATICFILES_FINDERS = STATICFILES_FINDERS + (
    'compressor.finders.CompressorFinder',
)


## URLS AND SUBDOMAINS
# https://docs.djangoproject.com/en/1.8/ref/settings/#root-urlconf.
# https://django-subdomains.readthedocs.org/en/latest/
ROOT_URLCONF = 'classmechanics.urls.brands'
SUBDOMAIN_URLCONFS = {
    None: 'classmechanics.urls.redirect_to_www',
    'accounts': 'classmechanics.urls.accounts',
    'admin': 'classmechanics.urls.admin',
    'stripe': 'classmechanics.urls.stripe',
    'www': 'classmechanics.urls.marketing',
}


# ANALYTICS
PIWIK_HOST = env('PIWIK_HOST', default=None)
PIWIK_SITE_ID = env('PIWIK_SITE_ID', default=None)


# AUTH
# http://django-allauth.readthedocs.org/en/latest/

ACCOUNT_ADAPTER = 'classmechanics.accountadapter.SubdomainAccountAdapter'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/profile/'
SESSION_COOKIE_DOMAIN = env('SESSION_COOKIE_DOMAIN', default=None)
SESSION_COOKIE_NAME = env('SESSION_COOKIE_NAME', default='sessionid')
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'METHOD': 'js_sdk',
        'SCOPE': ['email', 'public_profile'],
        'AUTH_PARAMS': {'auth_type': 'https'},
        'FIELDS': [
            'id',
            'email',
            'name',
            'first_name',
            'last_name',
            'verified',
            'locale',
            'timezone',
            'link',
            'updated_time',
            'friends',
        ],
        'EXCHANGE_TOKEN': True,
        'VERIFIED_EMAIL': False,
    },
    'stripe': {
        'SCOPE': ['read_write'],
    },
}


# SSL

from classmechanics.apps.organizations.sslify_disable import \
    disable_for_cname_request

SSLIFY_DISABLE_FOR_REQUEST = [
    disable_for_cname_request,
]


# MARKDOWN / MARKUPFIELD
# https://github.com/jamesturk/django-markupfield

import markdown
MARKUP_FIELD_TYPES = [
    ('markdown', markdown.markdown),
]


# Money
# https://pypi.python.org/pypi/django-money

CURRENCIES = ['USD']


# Images

FILE_UPLOAD_HANDLERS = [
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
]


# Messages

from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    messages.DEBUG: 'info',
    messages.INFO: 'info',
    messages.SUCCESS: 'success',
    messages.WARNING: 'warning',
    messages.ERROR: 'danger',
}


# Custom CSS

from shutil import which
LESSC_BIN_PATH = env('LESSC_BIN_PATH', default=which('lessc'))


# CSRF

CSRF_COOKIE_NAME = 'cmcsrftoken'


# FSM

FSM_ADMIN_FORCE_PERMIT = True
