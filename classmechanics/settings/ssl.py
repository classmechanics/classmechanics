def inject_sslify_middleware(middleware):
    # We need to check for SSL redirect *after* we've determine which
    # organization the host corresponds to.
    _i = middleware.index(
        'classmechanics.apps.brands.middleware.BrandMiddleware')
    _head, _tail = middleware[:_i], middleware[_i:]
    return _head + ['sslify.middleware.SSLifyMiddleware'] + _tail
