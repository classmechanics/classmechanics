from django.conf import settings


def allauth_providers_reverse():
    # Monkey patch allauth providers to use 'accounts' subdomain:
    from subdomains.utils import reverse as sdreverse
    def _accounts_reverse(*args, **kwargs):
        kwargs = dict(
            kwargs,
            subdomain='accounts',
            scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL,
        )
        return sdreverse(*args, **kwargs)
    from allauth.socialaccount.providers.facebook import provider as _facebook_provider
    from allauth.socialaccount.providers.oauth2 import provider as _oauth2_provider
    _facebook_provider.reverse = _accounts_reverse
    _oauth2_provider.reverse = _accounts_reverse
