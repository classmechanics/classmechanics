import unicodedata
from urllib.parse import urlparse

from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.sites.models import Site

from classmechanics.apps.brands.models import Cname


class SubdomainAccountAdapter(DefaultAccountAdapter):

    def is_safe_url(self, url):
        return is_safe_subdomain_url(url)


def is_safe_subdomain_url(url):
    """
    Return ``True`` if the url is a safe redirection (i.e. it doesn't point to
    a different host and uses a safe scheme).

    Always returns ``False`` on an empty url.
    """
    if url is not None:
        url = url.strip()
    if not url:
        return False
    # Chrome treats \ completely as /
    url = url.replace('\\', '/')
    # Chrome considers any URL with more than two slashes to be absolute, but
    # urlparse is not so flexible. Treat any url with three slashes as unsafe.
    if url.startswith('///'):
        return False
    url_info = urlparse(url)
    # Forbid URLs like http:///example.com - with a scheme, but without a hostname.
    # In that URL, example.com is not the hostname but, a path component. However,
    # Chrome will still consider example.com to be the hostname, so we must not
    # allow this syntax.
    if not url_info.netloc and url_info.scheme:
        return False
    # Forbid URLs that start with control characters. Some browsers (like
    # Chrome) ignore quite a few control characters at the start of a
    # URL and might consider the URL as scheme relative.
    if unicodedata.category(url[0])[0] == 'C':
        return False
    # Only allow subdomains that are a part of the current site's base domain.
    site = Site.objects.get_current()
    netloc = url_info.netloc[4:] if url_info.netloc.startswith('www.') else url_info.netloc
    netloc_is_subdomain = (
        (netloc == site.domain)
        or (netloc.endswith('.{}'.format(site.domain)))
        or Cname.objects.filter(fqdn=netloc).exists()
    )
    return ((not url_info.netloc or netloc_is_subdomain) and
            (not url_info.scheme or url_info.scheme in ['http', 'https']))
