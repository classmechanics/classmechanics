from django.conf.urls import include, url


urlpatterns = [
    url(r'', include('classmechanics.urls.base_robots')),
    url(r'', include('classmechanics.apps.stripepayments.urls')),
]
