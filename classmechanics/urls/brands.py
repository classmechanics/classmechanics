from django.conf.urls import include, url


urlpatterns = [
    url(r'', include('classmechanics.urls.base')),
    url(r'', include('classmechanics.urls.base_login_logout')),
    url(r'', include('classmechanics.apps.brands.urls')),
    url(r'', include('classmechanics.apps.brands.urls_catchall'))
]
