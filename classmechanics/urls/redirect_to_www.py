from django.conf.urls import url
from django.http import HttpResponsePermanentRedirect
from django.utils.http import urlquote


def redirect_to_www(request):
    host = request.get_host()
    new_url_parts = ['www.' + host, request.path]
    new_url = "{}://{}{}".format(
        request.scheme,
        new_url_parts[0],
        urlquote(new_url_parts[1]),
    )
    if request.META.get('QUERY_STRING', ''):
        new_url += '?' + request.META['QUERY_STRING']
    return HttpResponsePermanentRedirect(new_url)


urlpatterns = [
    url(r'', redirect_to_www),
]
