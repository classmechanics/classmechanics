from django.conf.urls import url, include


urlpatterns = [
    url(r'', include('classmechanics.urls.base')),
    url(r'', include('classmechanics.urls.base_robots')),
    url(r'', include('classmechanics.apps.accounts.urls')),
    url(r'^orders/', include('classmechanics.apps.orders.urls', 'orders')),
]
