""" Default urlconf for classmechanic """

from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static


urlpatterns = patterns(
    '',

    # brand-specific styles
    url(
        r'^(?P<slug>[-\w]+)\.(?P<timestamp>[\.\d]+)\.css$',
        'classmechanics.apps.brands.views.brand_css',
        name='brand_css',
    ),
)


if settings.DEBUG:
    # Add debug-toolbar
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

    # Serve media files through Django.
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT,
    )

    # Show error pages during development
    urlpatterns += patterns(
        '',
        url(r'^403/$', 'django.views.defaults.permission_denied'),
        url(r'^404/$', 'django.views.defaults.page_not_found'),
        url(r'^500/$', 'django.views.defaults.server_error'),
    )
