from django.conf import settings
from django.conf.urls import url
from django.views.generic import RedirectView
from subdomains.utils import reverse


class RedirectToLoginView(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        qs = self.request.META['QUERY_STRING']
        return '{}{}{}'.format(
            reverse('account_login', subdomain='accounts',
                    scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL),
            '?' if qs else '',
            qs or '',
        )


class RedirectToLogoutView(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        qs = self.request.META['QUERY_STRING']
        return '{}{}{}'.format(
            reverse('account_logout', subdomain='accounts',
                    scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL),
            '?' if qs else '',
            qs or '',
        )


urlpatterns = [
    # Redirect login and logout URLs to accounts subdomain.
    url(r"^login/$", RedirectToLoginView.as_view()),
    url(r"^logout/$", RedirectToLogoutView.as_view()),
]
