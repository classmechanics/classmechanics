from django.conf import settings
from django_behave.runner import DjangoBehaveTestSuiteRunner, DjangoBehaveOnlyTestSuiteRunner


class TestModeRunner(DjangoBehaveTestSuiteRunner):

    def __init__(self, *args, **kwargs):
        settings.TEST_MODE = True
        super(TestModeRunner, self).__init__(*args, **kwargs)


class BehaveOnlyTestModeRunner(DjangoBehaveOnlyTestSuiteRunner):

    def __init__(self, *args, **kwargs):
        settings.TEST_MODE = True
        super(BehaveOnlyTestModeRunner, self).__init__(*args, **kwargs)
