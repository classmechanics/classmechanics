from django import template
from django.db.models import Model
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from subdomains.templatetags.subdomainurls import url as sdurl


register = template.Library()


@register.filter
def content_type(obj):
    return ContentType.objects.get_for_model(obj) if obj else None


@register.filter
def is_a(object, modelname):
    """
    :type object: Model
    :type modelname: str
    """
    return modelname == '{}.{}'.format(object._meta.app_label,
                                       object._meta.object_name)


@register.inclusion_tag('_stage_notice.html')
def stage_notice():
    site = Site.objects.get_current()
    domain = site.domain.replace('.', '_').replace(':', '_')
    return {
        'stage_notice_template': '_stage_notice_{}.html'.format(domain),
    }


# Import subdomain URL tag as 'sdurl'
register.simple_tag(sdurl, takes_context=True, name='sdurl')
