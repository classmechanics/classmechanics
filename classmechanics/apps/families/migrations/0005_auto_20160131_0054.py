# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0010_auto_20160131_0054'),
        ('families', '0004_auto_20160131_0019'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agreement',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(verbose_name='modified', auto_now=True)),
                ('brand', models.ForeignKey(to='brands.Brand')),
                ('family', models.ForeignKey(to='families.Family')),
            ],
        ),
        migrations.AlterField(
            model_name='child',
            name='special_needs',
            field=models.CharField(max_length=200, verbose_name='Special needs / allergies', blank=True, null=True, help_text='\n            (optional)\n            Let us know if there are special needs,\n            allergies, or other considerations for this child.'),
        ),
        migrations.AlterUniqueTogether(
            name='agreement',
            unique_together=set([('family', 'brand')]),
        ),
    ]
