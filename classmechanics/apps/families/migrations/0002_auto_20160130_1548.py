# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='gender',
            field=models.CharField(choices=[('F', 'Female'), ('M', 'Male'), ('O', 'Other')], blank=True, null=True, max_length=1),
        ),
        migrations.AddField(
            model_name='member',
            name='special_needs',
            field=models.CharField(blank=True, null=True, max_length=200),
        ),
    ]
