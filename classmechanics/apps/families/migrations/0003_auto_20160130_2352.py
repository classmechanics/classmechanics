# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0002_auto_20160130_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='family',
            name='initial_profile_completed',
        ),
        migrations.AlterField(
            model_name='member',
            name='gender',
            field=models.CharField(null=True, max_length=1, blank=True, choices=[(None, ''), ('F', 'Female'), ('M', 'Male'), ('O', 'Other')]),
        ),
    ]
