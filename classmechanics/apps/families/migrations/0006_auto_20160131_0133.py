# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0005_auto_20160131_0054'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='child',
            options={'verbose_name_plural': 'Children'},
        ),
        migrations.AlterModelOptions(
            name='family',
            options={'verbose_name_plural': 'Families'},
        ),
        migrations.AlterField(
            model_name='family',
            name='name',
            field=models.CharField(max_length=200, help_text='\n            This helps us keep track of your family.\n            You can use your family name, or something fun of your choice.\n            Only you and our staff will see it.', verbose_name='Family Name'),
        ),
    ]
