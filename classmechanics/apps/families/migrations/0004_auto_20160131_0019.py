# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('families', '0003_auto_20160130_2352'),
    ]

    operations = [
        migrations.CreateModel(
            name='Adult',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Child',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('name', models.CharField(max_length=200)),
                ('birthdate', models.DateField(blank=True, null=True, help_text='\n            (optional)\n            We use this to suggest age-appropriate classes and camps.')),
                ('gender', models.CharField(choices=[(None, ''), ('F', 'Female'), ('M', 'Male'), ('O', 'Other')], blank=True, null=True, max_length=1, help_text='\n            (optional)\n            This helps us understand the diversity of students\n            who attend our classes and camps.')),
                ('special_needs', models.CharField(blank=True, null=True, max_length=200, help_text='\n            (optional)\n            Let us know if there are special needs\n            or considerations for this child.')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='member',
            name='family',
        ),
        migrations.RemoveField(
            model_name='member',
            name='user',
        ),
        migrations.AlterField(
            model_name='family',
            name='name',
            field=models.CharField(help_text='\n            This helps us keep track of your family.\n            You can use your family name, or something fun of your choice.\n            Only you and our staff will see it.', max_length=200),
        ),
        migrations.DeleteModel(
            name='Member',
        ),
        migrations.AddField(
            model_name='child',
            name='family',
            field=models.ForeignKey(to='families.Family'),
        ),
        migrations.AddField(
            model_name='child',
            name='user',
            field=models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='adult',
            name='family',
            field=models.ForeignKey(to='families.Family'),
        ),
        migrations.AddField(
            model_name='adult',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
