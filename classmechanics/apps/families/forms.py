from django import forms

from .models import Family, Child


class AgreementForm(forms.Form):

    is_authorized = forms.Field(
        widget=forms.CheckboxInput,
        label='Check here to agree to the following statements:',
    )

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('is_authorized'):
            msg = 'Please agree to these statements to continue.'
            self.add_error('is_authorized', msg)


class FamilyForm(forms.ModelForm):

    class Meta:
        model = Family
        fields = ['name']


class ChildForm(forms.ModelForm):

    class Meta:
        model = Child
        fields = ['name', 'birthdate', 'gender', 'special_needs']


ChildFormSet = forms.inlineformset_factory(
    Family,
    Child,
    form=ChildForm,
    min_num=1,
    max_num=10,
    extra=10,
    can_delete=False,
)
