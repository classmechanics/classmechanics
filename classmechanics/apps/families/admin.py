from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from .models import Adult, Agreement, Child, Family


def _admin_link(instance):
    if instance.id is not None:
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label,
                                              instance._meta.model_name),
                      args=(instance.id,))
        return format_html('<a href="{}">Details</a>', url)
    else:
        return ''


class AdultInline(admin.TabularInline):

    model = Adult
    extra = 0
    fields = ['name', 'user', 'modified']
    readonly_fields = ['name', 'modified']

    def name(self, instance):
        return '{0.first_name} {0.last_name}'.format(instance.user)


class AgreementInline(admin.TabularInline):

    model = Agreement
    extra = 0
    fields = ['brand', 'created']
    readonly_fields = ['brand', 'created']
    can_delete = False

    def has_add_permission(self, request):
        return False


class ChildInline(admin.TabularInline):

    model = Child
    extra = 0
    fields = ['name', 'admin_link', 'modified']
    readonly_fields = ['admin_link', 'modified']

    def admin_link(self, instance):
        return _admin_link(instance)


class ChildAdmin(admin.ModelAdmin):

    list_display = ['name', 'family']
    readonly_fields = ['created', 'modified']

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Child, ChildAdmin)


class FamilyAdmin(admin.ModelAdmin):

    list_display = ['name']
    readonly_fields = ['created', 'modified']
    inlines = [AdultInline, ChildInline, AgreementInline]

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Family, FamilyAdmin)
