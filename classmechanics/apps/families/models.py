from django.conf import settings
from django.db import models
from django_extensions.db.models import TimeStampedModel


class Adult(TimeStampedModel):
    """An adult member of the family who can authorize payments."""

    family = models.ForeignKey('Family')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)


class Agreement(TimeStampedModel):
    """Agreement between a family and a brand."""

    family = models.ForeignKey('Family')
    brand = models.ForeignKey('brands.Brand')

    class Meta:
        unique_together = ('family', 'brand')


class Child(TimeStampedModel):
    """A child member of the family, who can have a user account if 13+."""

    GENDER_CHOICES = [
        (None, ''),
        ('F', 'Female'),
        ('M', 'Male'),
        ('O', 'Other'),
    ]

    family = models.ForeignKey('Family')
    name = models.CharField(max_length=200)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    birthdate = models.DateField(
        blank=True, null=True,
        help_text="""
            (optional)
            We use this to suggest age-appropriate classes and camps.""")
    gender = models.CharField(
        max_length=1, choices=GENDER_CHOICES, blank=True, null=True,
        help_text="""
            (optional)
            This helps us understand the diversity of students
            who attend our classes and camps.""")
    special_needs = models.CharField(
        'Special needs / allergies',
        max_length=200, blank=True, null=True,
        help_text="""
            (optional)
            Let us know if there are special needs,
            allergies, or other considerations for this child.""")

    class Meta:
        verbose_name_plural = 'Children'

    def __str__(self):
        return self.name


class Family(TimeStampedModel):
    """A group of adults and children who pay for classes and camps together."""

    name = models.CharField(
        'Family Name',
        max_length=200,
        help_text="""
            This helps us keep track of your family.
            You can use your family name, or something fun of your choice.
            Only you and our staff will see it.""")

    class Meta:
        verbose_name_plural = 'Families'

    def __str__(self):
        return self.name
