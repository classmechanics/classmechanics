class FamilyMiddleware(object):

    def process_request(self, request):
        user = request.user
        request.family = None
        if user.is_authenticated():
            adult = request.user.adult_set.first()
            if adult:
                request.family = adult.family
