from urllib.parse import urlsplit

from django.conf import settings
from django.contrib.sites.models import Site
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404

from .models import Brand, Cname


class BrandMiddleware(object):

    def process_request(self, request):
        site = Site.objects.get_current()
        host = request.META['HTTP_HOST']
        request.brand = None
        request.cname = None
        request.organization = None
        is_site_subdomain = (
            host == site.domain
            or host.endswith('.' + site.domain)
        )
        if not is_site_subdomain:
            if host.startswith('www.'):
                fqdn = host[4:]  # strip www prefix for lookup
            else:
                fqdn = host
            cname = Cname.objects.filter(fqdn=fqdn).first()
            if cname:
                request.brand = cname.brand
                request.cname = cname
                request.session['last_subdomain'] = request.brand.site_name
                if host.startswith('www.'):
                    # serve up org site
                    request.urlconf = settings.ROOT_URLCONF
                else:
                    # redirect to www prefix
                    request.urlconf = settings.SUBDOMAIN_URLCONFS[None]
            else:
                # invalid host
                return HttpResponseBadRequest()
        elif request.subdomain not in settings.SUBDOMAIN_URLCONFS:
            request.brand = get_object_or_404(
                Brand, site_name=request.subdomain)
            request.session['last_subdomain'] = request.subdomain
        elif (request.subdomain == 'accounts'
              and ('HTTP_REFERER' in request.META
                   or 'last_subdomain' in request.session)
              ):
            referrer = request.META.get('HTTP_REFERER')
            if referrer is not None:
                ref_netloc = urlsplit(referrer).netloc
                if ref_netloc.startswith('www.'):
                    ref_netloc = ref_netloc[4:]
                cname = Cname.objects.filter(fqdn=ref_netloc).first()
                if cname:
                    request.brand = cname.brand
                    request.session['last_subdomain'] = request.brand.site_name
                else:
                    site_name = ref_netloc[:-len(site.domain)-1]  # strip domain suffix
                    brand = Brand.objects.filter(site_name=site_name).first()
                    if brand:
                        request.brand = brand
                        request.session['last_subdomain'] = site_name
            last_subdomain = request.session.get('last_subdomain')
            if last_subdomain not in settings.SUBDOMAIN_URLCONFS:
                request.brand = Brand.objects.get(site_name=last_subdomain)
        else:
            request.session['last_subdomain'] = request.subdomain
        if request.brand:
            request.organization = request.brand.organization
