import cloudinary.models
from colorfield.fields import ColorField
from django.db import models
from django_extensions.db.models import TimeStampedModel
from timezone_field import TimeZoneField

from classmechanics.apps.pages.models import NavigationLink


class Appearance(TimeStampedModel):
    """The visual identity of a brand."""

    brand = models.OneToOneField('Brand')
    logo = cloudinary.models.CloudinaryField('logo', blank=True, null=True)
    body_font = models.CharField(max_length=50, blank=True, null=True)
    heading_font = models.CharField(max_length=50, blank=True, null=True)
    font_base_size = models.PositiveIntegerField(default=14, help_text='in pixels')
    line_height = models.DecimalField(default='1.42', max_digits=3, decimal_places=2)
    color_1 = ColorField(
        default='#ffffff',
        help_text='body background')
    color_2 = ColorField(
        default='#eeeeee',
        help_text='disabled input & button background, input addon background, nav & tabs & pagination link hover background, jumbotron background')
    color_3 = ColorField(
        default='#777777',
        help_text='disabled link color, input placeholder color, dropdown header color, navbar inverse & link color')
    color_4 = ColorField(
        default='#555555',
        help_text='nav tab link hover color')
    color_5 = ColorField(
        default='#337ab7',
        help_text='link color, primary button background, pagination active background, progress bar background, label background, panel heading color')
    color_6 = ColorField(
        default='#333333',
        help_text='text color, legend color, dropdown link color, panel text color, code color')
    color_7 = ColorField(
        default='#222222',
        help_text='navbar inverse background')

    def __str__(self):
        return 'Appearance for {}'.format(self.brand.name)


class Brand(TimeStampedModel):
    """A brand controlled by an organization."""

    organization = models.ForeignKey('organizations.Organization')
    name = models.CharField(max_length=200)
    site_name = models.CharField(max_length=50, unique=True)
    default_timezone = TimeZoneField(default='US/Central')
    live = models.BooleanField(
        default=False,
        help_text='Live sites will be indexed by search engines.')

    def __str__(self):
        return '{0.site_name}: {0.name}'.format(self)

    def navbar_navigation_links(self):
        return NavigationLink.objects.filter(
            page__brand=self,
            position='navbar',
        )

    def footer_navigation_links(self):
        return NavigationLink.objects.filter(
            page__brand=self,
            position='footer',
        )


class Cname(TimeStampedModel):
    """A DNS alias for the brand."""

    brand = models.OneToOneField('Brand')
    fqdn = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return '{0.fqdn} -> {0.brand.site_name}'.format(self)
