from django.contrib import admin
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from classmechanics.apps.pages.models import Page
from .models import Brand, Cname, Appearance


class AppearanceInline(admin.StackedInline):

    model = Appearance
    exclude = ['created']
    readonly_fields = ['modified']


class CnameInline(admin.TabularInline):

    model = Cname
    exclude = ['created']
    readonly_fields = ['modified']


class PageInline(admin.TabularInline):

    def page_link(self, instance):
        if instance.id is not None:
            page = instance
            url = reverse(
                'admin:%s_%s_change' % (page._meta.app_label,
                                        page._meta.model_name),
                args=(page.id,))
            return format_html('<a href="{}">Details</a>', url)

    model = Page
    fields = ['title', 'slug', 'page_link', 'modified']
    readonly_fields = ['page_link', 'modified']
    extra = 1
    ordering = ['slug']


class BrandAdmin(admin.ModelAdmin):

    def organization_link(self, instance):
        if instance.id is not None:
            organization = instance.organization
            url = reverse(
                'admin:%s_%s_change' % (organization._meta.app_label,
                                        organization._meta.model_name),
                args=(organization.id,))
            return format_html('<a href="{}">{}</a>', url, organization.name)

    def site_link(self, instance):
        if instance.id is not None:
            site = Site.objects.get_current()
            url = '//{}.{}/'.format(instance.site_name, site.domain)
            return format_html('<a href="{}">{}</a>', url, instance.site_name)

    def website_link(self, instance):
        if instance.id is not None and hasattr(instance, 'cname'):
            fqdn = instance.cname.fqdn
            url = 'http://www.{}'.format(fqdn)
            return format_html('<a href="{}">www.{}</a>', url, fqdn)

    list_display = ['name', 'organization_link', 'site_link', 'website_link',
                    'modified']
    readonly_fields = ['organization_link', 'site_link', 'website_link',
                       'created', 'modified']
    inlines = [CnameInline, AppearanceInline, PageInline]

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Brand, BrandAdmin)
