from django.conf.urls import include, url


urlpatterns = [
    url(r'', include('classmechanics.apps.pages.urls', 'pages')),
]
