from django.conf.urls import include, url


urlpatterns = [
    url(r'^robots.txt$', 'classmechanics.apps.brands.views.robots_txt'),

    url(r'^camps/', include('classmechanics.apps.camps.urls', 'camps')),
    url(r'^classes/', include('classmechanics.apps.classes.urls', 'classes')),
    url(r'^instructors/', include('classmechanics.apps.instructors.urls', 'instructors')),
    url(r'^locations/', include('classmechanics.apps.locations.urls', 'locations')),
]
