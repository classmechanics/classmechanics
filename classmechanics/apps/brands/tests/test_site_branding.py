from django.contrib.sites.models import Site
from django.test import TestCase
from model_mommy import mommy
from pyquery import PyQuery as PQ

from classmechanics.tests.lib.secureclient import SecureClient

from ..models import Brand, Cname


class SiteBrandingTestCase(TestCase):

    def setUp(self):
        self.c = SecureClient()
        self.brand = mommy.make(
            Brand, name='Acme Business Classes', site_name='abc')
        self.site = Site.objects.get_current()
        self.site.name = 'Site Name'
        self.site.save()

    def test__brand_exists__site_branded(self):
        # Global site name should not appear in nav.
        response = self.c.get('/', HTTP_HOST='abc.example.com')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertNotIn(self.site.name, nav)
        # Instead, the brand name should appear instead.
        self.assertIn(self.brand.name, nav)

    def test__no_brand_exists__generic_accounts_nav(self):
        response = self.c.get('/login/', HTTP_HOST='accounts.example.com')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertIn(self.site.name, nav)

    def test__brand_exists__site_branded_accounts_nav(self):
        self.c.get('/', HTTP_HOST='abc.example.com')
        response = self.c.get('/login/', HTTP_HOST='accounts.example.com')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertNotIn(self.site.name, nav)
        self.assertIn(self.brand.name, nav)

    def test__brand_exists__www_visit__generic_accounts_nav(self):
        self.c.get('/', HTTP_HOST='abc.example.com')
        self.c.get('/', HTTP_HOST='www.example.com')
        response = self.c.get('/login/', HTTP_HOST='accounts.example.com')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertIn(self.site.name, nav)
        self.assertNotIn(self.brand.name, nav)

    def test__brand_exists__site_referrer__branded_accounts_nav(self):
        response = self.c.get('/login/', HTTP_HOST='accounts.example.com',
                              HTTP_REFERER='http://www.abc.example.com/classes/')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertNotIn(self.site.name, nav)
        self.assertIn(self.brand.name, nav)

    def test__brand_exists__cname_referrer__branded_accounts_nav(self):
        cname = Cname.objects.create(brand=self.brand, fqdn='xyz.com')
        response = self.c.get('/login/', HTTP_HOST='accounts.example.com',
                              HTTP_REFERER='http://www.xyz.com/classes/')
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertNotIn(self.site.name, nav)
        self.assertIn(self.brand.name, nav)
