from django.test import TestCase
from model_mommy import mommy

from classmechanics.tests.lib.secureclient import SecureClient

from ..models import Brand


class RedirectsTestCase(TestCase):

    def setUp(self):
        self.c = SecureClient()
        self.brand = mommy.make(Brand, name='ABC', site_name='abc')

    def test__org_exists__200(self):
        response = self.c.get('/', HTTP_HOST='abc.example.com')
        self.assertEqual(200, response.status_code)

    def test__org_does_not_exist__404(self):
        response = self.c.get('/', HTTP_HOST='xyz.example.com')
        self.assertEqual(404, response.status_code)
