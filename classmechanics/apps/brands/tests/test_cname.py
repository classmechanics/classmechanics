from django.test import Client, TestCase
from model_mommy import mommy
from pyquery import PyQuery as PQ

from ..models import Brand, Cname


class CnameTestCase(TestCase):

    def setUp(self):
        self.c = Client()
        self.brand = mommy.make(Brand)
        self.cname = Cname.objects.create(brand=self.brand, fqdn='xyz.com')

    def test__http__cname__brand_exists__no_www__redirect_to_www(self):
        response = self.c.get('/', secure=False, HTTP_HOST='xyz.com')
        self.assertEqual('http://www.xyz.com/', response.url)

    def test__http__cname__brand_exists__www__serve_brand_site(self):
        response = self.c.get('/', secure=False, HTTP_HOST='www.xyz.com')
        self.assertEqual(200, response.status_code)
        doc = PQ(response.content)
        nav = doc.find('nav:first').__html__()
        self.assertIn(self.brand.name, nav)

    def test__http__cname__brand_not_found__400_error(self):
        response = self.c.get('/', secure=False, HTTP_HOST='www.xyzzy.com')
        self.assertEqual(400, response.status_code)
