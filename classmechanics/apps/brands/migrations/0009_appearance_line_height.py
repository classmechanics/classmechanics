# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0008_auto_20160128_0847'),
    ]

    operations = [
        migrations.AddField(
            model_name='appearance',
            name='line_height',
            field=models.DecimalField(default='1.42', decimal_places=2, help_text='in pixels', max_digits=3),
        ),
    ]
