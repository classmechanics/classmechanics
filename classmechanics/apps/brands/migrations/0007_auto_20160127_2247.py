# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import cloudinary.models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0006_auto_20160127_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appearance',
            name='logo',
            field=cloudinary.models.CloudinaryField(null=True, max_length=255, blank=True, verbose_name='logo'),
        ),
    ]
