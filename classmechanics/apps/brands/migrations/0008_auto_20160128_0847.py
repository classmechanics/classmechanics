# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0007_auto_20160127_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='appearance',
            name='color_1',
            field=colorfield.fields.ColorField(default='#ffffff', help_text='body background', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_2',
            field=colorfield.fields.ColorField(default='#eeeeee', help_text='disabled input & button background, input addon background, nav & tabs & pagination link hover background, jumbotron background', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_3',
            field=colorfield.fields.ColorField(default='#777777', help_text='disabled link color, input placeholder color, dropdown header color, navbar inverse & link color', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_4',
            field=colorfield.fields.ColorField(default='#555555', help_text='nav tab link hover color', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_5',
            field=colorfield.fields.ColorField(default='#337ab7', help_text='link color, primary button background, pagination active background, progress bar background, label background, panel heading color', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_6',
            field=colorfield.fields.ColorField(default='#333333', help_text='text color, legend color, dropdown link color, panel text color, code color', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='color_7',
            field=colorfield.fields.ColorField(default='#222222', help_text='navbar inverse background', max_length=10),
        ),
        migrations.AddField(
            model_name='appearance',
            name='font_base_size',
            field=models.PositiveIntegerField(default=14, help_text='in pixels'),
        ),
    ]
