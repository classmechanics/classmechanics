# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0005_auto_20160127_2035'),
    ]

    operations = [
        migrations.AddField(
            model_name='appearance',
            name='body_font',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='appearance',
            name='heading_font',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
