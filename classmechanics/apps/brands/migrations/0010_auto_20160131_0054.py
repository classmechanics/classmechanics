# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0009_appearance_line_height'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appearance',
            name='line_height',
            field=models.DecimalField(max_digits=3, decimal_places=2, default='1.42'),
        ),
    ]
