# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields
import cloudinary.models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0004_identity'),
    ]

    operations = [
        migrations.RenameModel('Identity', 'Appearance'),
    ]
