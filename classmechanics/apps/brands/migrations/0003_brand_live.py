# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0002_brand_default_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='live',
            field=models.BooleanField(default=False, help_text='Live sites will be indexed by search engines.'),
        ),
    ]
