import os
from subprocess import check_output

from django.conf import settings
from django.core.cache import caches
from django.http import HttpResponse
from django.views.decorators.cache import cache_control
from django.views.decorators.http import last_modified


BRAND_CSS_MAX_AGE = 60 * 60 * 24 * 365 * 4

LIVE = """\
User-agent: *
Allow: /
"""

NOT_LIVE = """\
User-agent: *
Disallow: /
"""

LESS_ROOT = os.path.join(settings.BASE_DIR, 'static', 'less')

LESS_TEMPLATE = """\
@import "../vendor/bootstrap/less/bootstrap.less";
@import "app.less";
@icon-font-path: "{static_url}/vendor/bootstrap/fonts/";
@font-base-size: {appearance.font_base_size}px !default;
@line-height-base: {appearance.line_height};
@body-bg: {appearance.color_1};
@gray-lighter: {appearance.color_2};
@gray-light: {appearance.color_3};
@gray: {appearance.color_4};
@brand-primary: {appearance.color_5};
@gray-dark: {appearance.color_6};
@navbar-inverse-bg: {appearance.color_7};
"""


def robots_txt(request):
    if not request.brand.live:
        content = NOT_LIVE
    else:
        if request.cname is not None:
            # Browsing via CNAME.
            content = LIVE
        elif hasattr(request.brand, 'cname'):
            # Browsing directly to site that has a CNAME.
            content = NOT_LIVE
        else:
            # Browsing directly to site that doesn't have a CNAME.
            content = LIVE
    return HttpResponse(content=content, content_type='text/plain')


def brand_css_last_modified(request, slug, timestamp):
    return request.brand.appearance.modified


@cache_control(max_age=BRAND_CSS_MAX_AGE)
@last_modified(brand_css_last_modified)
def brand_css(request, slug, timestamp):
    brand = request.brand
    appearance = brand.appearance
    cache_key = 'brand-appearance--{}--{}'.format(
        brand.site_name, int(appearance.modified.timestamp()))
    cache = caches['default']
    content = cache.get(cache_key)
    if content is None:
        lessc_args = [
            # standard options
            settings.LESSC_BIN_PATH,
        ] + ([] if settings.DEBUG else [
            # additional production options
            '--clean-css',
        ]) + [
            # positional args
            '-',
        ]
        lessc_stdin = LESS_TEMPLATE.format(
            static_url=settings.STATIC_URL,
            appearance=request.brand.appearance,
        ).encode('utf8')
        content = check_output(lessc_args, timeout=10, input=lessc_stdin, cwd=LESS_ROOT)
        cache.set(cache_key, content)
    return HttpResponse(content=content, content_type='text/css')
