from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from .models import Instructor


class InstructorAdmin(admin.ModelAdmin):

    def organization_link(self, instance):
        if instance.id is not None:
            organization = instance.organization
            url = reverse(
                'admin:%s_%s_change' % (organization._meta.app_label,
                                        organization._meta.model_name),
                args=(organization.id,))
            return format_html('<a href="{}">{}</a>', url, organization.name)

    list_display = ['name', 'organization_link', 'modified']
    readonly_fields = ['organization_link', 'created', 'modified']

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Instructor, InstructorAdmin)
