from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, RedirectView

from .models import Instructor


class InstructorDetailView(DetailView):

    model = Instructor
    context_object_name = 'instructor'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(organization=self.request.organization)
        return qs


class InstructorRedirectView(RedirectView):

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        instructor = get_object_or_404(Instructor, pk=self.kwargs['pk'])
        return instructor.get_detail_url(brand=self.request.brand)
