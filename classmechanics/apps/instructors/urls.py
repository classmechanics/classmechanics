from django.conf.urls import url

from .views import InstructorDetailView, InstructorRedirectView


urlpatterns = [
    url(
        r'^(?P<pk>\d+)/$',
        InstructorRedirectView.as_view(),
    ),
    url(
        r'^(?P<pk>\d+)-(?P<slug>[\w-]+)/$',
        InstructorDetailView.as_view(),
        name='detail',
    ),
]
