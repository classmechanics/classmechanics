from django.db import models
from django.utils.text import slugify
from django_extensions.db.models import TimeStampedModel
from markupfield.fields import MarkupField
from subdomains.utils import reverse


class Instructor(TimeStampedModel):
    """A class with 1+ sessions, on a single day, at a single location."""

    organization = models.ForeignKey('organizations.Organization')
    name = models.CharField(max_length=200)
    bio = MarkupField(default_markup_type='markdown')

    def __str__(self):
        return self.name

    def get_detail_url(self, brand=None, subdomain=None):
        return reverse(
            'instructors:detail',
            subdomain=(subdomain if brand is None else brand.site_name),
            kwargs=dict(
                pk=self.pk,
                slug=slugify(self.name),
            ),
        )
