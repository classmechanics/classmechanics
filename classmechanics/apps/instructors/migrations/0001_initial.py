# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import markupfield.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0005_auto_20160125_0955'),
    ]

    operations = [
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(verbose_name='created', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(verbose_name='modified', auto_now=True)),
                ('name', models.CharField(max_length=200)),
                ('bio', markupfield.fields.MarkupField(rendered_field=True)),
                ('bio_markup_type', models.CharField(default='markdown', choices=[('', '--'), ('markdown', 'markdown')], max_length=30)),
                ('_bio_rendered', models.TextField(editable=False)),
                ('organization', models.ForeignKey(to='organizations.Organization')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
    ]
