from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


@require_POST
@csrf_exempt
def account_webhook(request):
    print('account', request.body)
    return HttpResponse('OK')


@require_POST
@csrf_exempt
def connect_webhook(request):
    print('connect', request.body)
    return HttpResponse('OK')
