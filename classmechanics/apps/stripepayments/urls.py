from django.conf.urls import url

urlpatterns = [
    url(r'^webhook/account/$', 'classmechanics.apps.stripepayments.views.account_webhook'),
    url(r'^webhook/connect/$', 'classmechanics.apps.stripepayments.views.connect_webhook'),
]
