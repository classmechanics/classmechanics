from django.db import models
from django_extensions.db.models import TimeStampedModel


class OrganizationStripeAccountHolder(TimeStampedModel):
    """
    The Stripe account holding user of an organization.
    """

    organization = models.OneToOneField('organizations.Organization')
    user = models.ForeignKey('auth.User')

    class Meta:
        unique_together = ('organization', 'user')
