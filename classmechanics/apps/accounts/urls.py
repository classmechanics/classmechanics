from django.conf import settings
from django.conf.urls import url, include
from django.views.generic import TemplateView, RedirectView
from subdomains.utils import reverse

from classmechanics.monkeypatches import allauth_providers_reverse
allauth_providers_reverse()


class RedirectToProfileView(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        return reverse('profile_index', subdomain='accounts',
                       scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL)


# Standard URLs need to be appended, not prepended,
# so that the actual allauth login/logout URLs are used
# instead of the redirectors that are in .base.urlpatterns
urlpatterns = [
    url(
        r'^$',
        RedirectToProfileView.as_view(),
        name='accounts_index',
    ),
    url(
        r'^profile/$',
        TemplateView.as_view(template_name='profile/index.html'),
        name='profile_index',
    ),
    url(
        r'^registrations/',
        include('classmechanics.apps.registrations.urls', 'registrations'),
    ),
    url(
        r'',
        include('allauth.urls'),
    ),
]
