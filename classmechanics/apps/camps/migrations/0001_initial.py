# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import djmoney.models.fields
from decimal import Decimal
import markupfield.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_auto_20160125_2046'),
        ('instructors', '0001_initial'),
        ('brands', '0003_brand_live'),
    ]

    operations = [
        migrations.CreateModel(
            name='Camp',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(verbose_name='created', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('name', models.CharField(max_length=200)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('min_age', models.PositiveIntegerField()),
                ('max_age', models.PositiveIntegerField(null=True, blank=True)),
                ('price_currency', djmoney.models.fields.CurrencyField(max_length=3, choices=[('USD', 'US Dollar')], default='USD', editable=False)),
                ('price', djmoney.models.fields.MoneyField(decimal_places=2, max_digits=8, default_currency='USD', default=Decimal('0.0'))),
                ('published', models.BooleanField()),
                ('description', markupfield.fields.MarkupField(rendered_field=True)),
                ('description_markup_type', models.CharField(max_length=30, choices=[('', '--'), ('markdown', 'markdown')], default='markdown')),
                ('_description_rendered', models.TextField(editable=False)),
                ('brand', models.ForeignKey(to='brands.Brand')),
                ('instructors', models.ManyToManyField(to='instructors.Instructor')),
                ('location', models.ForeignKey(to='locations.Location')),
            ],
            options={
                'get_latest_by': 'modified',
                'abstract': False,
                'ordering': ('-modified', '-created'),
            },
        ),
    ]
