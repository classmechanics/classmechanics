import cloudinary.models
from django.db import models
from django.utils.text import slugify
from django_extensions.db.models import TimeStampedModel
from djmoney.models.fields import MoneyField
from markupfield.fields import MarkupField
from subdomains.utils import reverse


class Appearance(TimeStampedModel):
    """The visual identity of a brand."""

    camp = models.OneToOneField('Camp')
    image = cloudinary.models.CloudinaryField('image', blank=True, null=True)

    def __str__(self):
        return 'Appearance for {}'.format(self.camp.name)


class Camp(TimeStampedModel):
    """A camp spanning several days, at a single location."""

    brand = models.ForeignKey('brands.Brand')
    name = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    min_age = models.PositiveIntegerField()
    max_age = models.PositiveIntegerField(blank=True, null=True)
    price = MoneyField(max_digits=8, decimal_places=2, default_currency='USD')
    instructors = models.ManyToManyField('instructors.Instructor')
    location = models.ForeignKey('locations.Location')
    published = models.BooleanField()
    description = MarkupField(default_markup_type='markdown')

    class Meta:
        ordering = ('start_date',)

    def __str__(self):
        return self.name

    def get_detail_url(self, brand=None, subdomain=None):
        return reverse(
            'camps:detail',
            subdomain=(subdomain if brand is None else brand.site_name),
            kwargs=dict(
                pk=self.pk,
                slug=slugify(self.name),
            ),
        )
