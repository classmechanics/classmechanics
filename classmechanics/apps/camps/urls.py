from django.conf.urls import url

from .views import CampDetailView, CampRedirectView


urlpatterns = [
    url(
        r'^(?P<pk>\d+)/$',
        CampRedirectView.as_view(),
    ),
    url(
        r'^(?P<pk>\d+)-(?P<slug>[\w-]+)/$',
        CampDetailView.as_view(),
        name='detail',
    ),
]
