from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, RedirectView

from .models import Camp


class CampDetailView(DetailView):

    model = Camp

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(brand__organization=self.request.organization)
        return qs


class CampRedirectView(RedirectView):

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        camp = get_object_or_404(Camp, pk=self.kwargs['pk'])
        return camp.get_detail_url(brand=self.request.brand)
