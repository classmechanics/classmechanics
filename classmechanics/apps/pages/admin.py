from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from .models import Appearance, Page, NavigationLink


class AppearanceInline(admin.StackedInline):

    model = Appearance
    fields = ['hero_image']
    readonly_fields = ['modified']


class NavigationLinkInline(admin.TabularInline):

    fields = ['position', 'order', 'text', 'modified']
    readonly_fields = ['modified']
    model = NavigationLink


class PageAdmin(admin.ModelAdmin):

    def brand_link(self, instance):
        if instance.id is not None:
            brand = instance.brand
            url = reverse(
                'admin:%s_%s_change' % (brand._meta.app_label,
                                        brand._meta.model_name),
                args=(brand.id,))
            return format_html('<a href="{}">{}</a>', url, brand.site_name)

    list_display = ['title', 'slug', 'published', 'brand_link', 'modified']
    readonly_fields = ['brand_link', 'created', 'modified']
    inlines = [AppearanceInline, NavigationLinkInline]

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Page, PageAdmin)
