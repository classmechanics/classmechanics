from django.http import Http404
from django.template import Template
from django.template.loader import get_template
from django.views.generic import DetailView

from .models import Page


PLACEHOLDER_NAMES = {'CAMPS', 'CLASSES'}


class PageDetailView(DetailView):

    model = Page

    def get_object(self, queryset=None):
        qs = queryset or self.get_queryset()
        slug = self.kwargs['slug']
        page = qs.filter(slug=slug).first()
        if page is None:
            if slug == '':
                brand = self.request.brand
                page = Page(
                    brand=brand,
                    title='Coming Soon: {}'.format(brand.name),
                    slug='',
                    published=True,
                    content='# Coming Soon!'
                )
                page._meta.get_field('content').pre_save(page, True)
            else:
                raise Http404()
        return page

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(brand=self.request.brand, published=True)
        return qs

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        content = str(data['page'].content)
        for placeholder_name in PLACEHOLDER_NAMES:
            placeholder_tag = '<p>~~{}~~</p>'.format(placeholder_name)
            if placeholder_tag in content:
                # noinspection PyUnresolvedReferences
                template_name = 'pages/placeholders/{}.html'.format(placeholder_name)
                template = get_template(template_name)
                context = {'request': self.request}
                placeholder_content = template.render(context, self.request)
                content = content.replace(
                    placeholder_tag,
                    placeholder_content,
                )
        data['page_content'] = content
        return data
