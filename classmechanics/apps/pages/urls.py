from django.conf.urls import url

from .views import PageDetailView


urlpatterns = [
    url(r'^$', PageDetailView.as_view(), name='index', kwargs=dict(slug='')),
    url(r'^(?P<slug>[-\w]+)/$', PageDetailView.as_view(), name='detail'),
]
