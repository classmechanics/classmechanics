from django import template

from ..models import Page


register = template.Library()


@register.filter
def index_page(brand):
    return Page.objects.filter(brand=brand, slug='').first()
