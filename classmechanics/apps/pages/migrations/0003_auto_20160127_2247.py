# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import cloudinary.models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_appearance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appearance',
            name='hero_image',
            field=cloudinary.models.CloudinaryField(null=True, max_length=255, blank=True, verbose_name='hero_image'),
        ),
    ]
