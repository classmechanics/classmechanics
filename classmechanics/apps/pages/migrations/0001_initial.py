# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import markupfield.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0003_brand_live'),
    ]

    operations = [
        migrations.CreateModel(
            name='NavigationLink',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(verbose_name='modified', auto_now=True)),
                ('position', models.CharField(max_length=6, db_index=True, choices=[('navbar', 'Navbar'), ('footer', 'Footer')])),
                ('order', models.PositiveIntegerField(default=1)),
                ('text', models.CharField(max_length=30)),
            ],
            options={
                'ordering': ('position', 'order'),
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(verbose_name='modified', auto_now=True)),
                ('title', models.CharField(max_length=60)),
                ('slug', models.SlugField(max_length=60, blank=True)),
                ('content', markupfield.fields.MarkupField(rendered_field=True)),
                ('published', models.BooleanField(default=False)),
                ('content_markup_type', models.CharField(default='markdown', max_length=30, choices=[('', '--'), ('markdown', 'markdown')])),
                ('_content_rendered', models.TextField(editable=False)),
                ('brand', models.ForeignKey(to='brands.Brand')),
            ],
        ),
        migrations.AddField(
            model_name='navigationlink',
            name='page',
            field=models.OneToOneField(to='pages.Page'),
        ),
        migrations.AlterUniqueTogether(
            name='page',
            unique_together=set([('brand', 'slug')]),
        ),
    ]
