import cloudinary.models
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django_extensions.db.models import TimeStampedModel
from markupfield.fields import MarkupField


class Appearance(TimeStampedModel):
    """The visual identity of a brand."""

    page = models.OneToOneField('Page')
    hero_image = cloudinary.models.CloudinaryField('hero_image', blank=True, null=True)

    def __str__(self):
        return 'Appearance for {}'.format(self.page.title)


class NavigationLink(TimeStampedModel):

    position = models.CharField(
        max_length=6,
        choices=[('navbar', 'Navbar'), ('footer', 'Footer')],
        db_index=True)
    order = models.PositiveIntegerField(default=1)
    page = models.OneToOneField('Page')
    text = models.CharField(max_length=30)

    class Meta:
        ordering = ('position', 'order')


class Page(TimeStampedModel):

    brand = models.ForeignKey('brands.Brand')
    title = models.CharField(max_length=60)
    slug = models.SlugField(max_length=60, blank=True)
    published = models.BooleanField(default=False)
    content = MarkupField(default_markup_type='markdown')

    class Meta:
        unique_together = [
            ('brand', 'slug'),
        ]

    def __str__(self):
        return self.title

    def absolute_url(self):
        site = Site.objects.get_current()
        if hasattr(self.brand, 'cname'):
            prefix = 'http://www.' + self.brand.cname.fqdn
        else:
            prefix = '{}://{}.{}'.format(
                settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL,
                self.brand.site_name,
                site.domain,
            )
        url = self.relative_url()
        return '{}{}'.format(prefix, url)

    def relative_url(self):
        urlconf = 'classmechanics.urls.brands'
        if self.slug == '':
            return reverse('pages:index', urlconf)
        else:
            return reverse('pages:detail', urlconf, kwargs=dict(slug=self.slug))
