# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0003_cname'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cname',
            name='organization',
        ),
        migrations.AlterUniqueTogether(
            name='owner',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='owner',
            name='organization',
        ),
        migrations.RemoveField(
            model_name='owner',
            name='user',
        ),
        migrations.DeleteModel(
            name='Cname',
        ),
        migrations.DeleteModel(
            name='Organization',
        ),
        migrations.DeleteModel(
            name='Owner',
        ),
    ]
