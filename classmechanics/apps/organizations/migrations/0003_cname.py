# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0002_auto_20160124_1732'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cname',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('fqdn', models.CharField(max_length=100, db_index=True, unique=True)),
                ('organization', models.OneToOneField(to='organizations.Organization')),
            ],
        ),
    ]
