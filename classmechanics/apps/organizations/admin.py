from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from classmechanics.apps.brands.models import Brand
from classmechanics.apps.locations.models import Location
from classmechanics.apps.stripepayments.models import \
    OrganizationStripeAccountHolder

from .models import Manager, Organization


def _admin_link(instance):
    if instance.id is not None:
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label,
                                              instance._meta.model_name),
                      args=(instance.id,))
        return format_html('<a href="{}">Details</a>', url)
    else:
        return ''


class BrandInline(admin.TabularInline):

    model = Brand
    extra = 1
    fields = ['name', 'site_name', 'admin_link']
    readonly_fields = ['admin_link']

    def admin_link(self, instance):
        return _admin_link(instance)


class LocationInline(admin.TabularInline):

    model = Location
    extra = 1
    fields = ['name', 'admin_link']
    readonly_fields = ['admin_link']

    def admin_link(self, instance):
        return _admin_link(instance)


class ManagerInline(admin.TabularInline):

    fields = ['user', 'admin_link']
    model = Manager
    extra = 1
    readonly_fields = ['admin_link']

    def admin_link(self, instance):
        return _admin_link(instance.user)


class StripeAccountHolderInline(admin.TabularInline):

    model = OrganizationStripeAccountHolder
    fields = ['user']


class OrganizationAdmin(admin.ModelAdmin):

    list_display = ['name']
    inlines = [ManagerInline, BrandInline, LocationInline,
               StripeAccountHolderInline]
    readonly_fields = ['created', 'modified']

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Organization, OrganizationAdmin)
