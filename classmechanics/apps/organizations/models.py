from django.db import models
from django.conf import settings
from django_extensions.db.models import TimeStampedModel


class Manager(TimeStampedModel):
    """Someone who has authority to manage organization details."""

    organization = models.ForeignKey('Organization')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta:
        unique_together = [
            ('organization', 'user'),
        ]


class Organization(TimeStampedModel):
    """A business entity that controls one or more brands."""

    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
