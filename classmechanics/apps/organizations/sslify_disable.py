def disable_for_cname_request(request):
    # Must import here as this module is imported in ...settings.base
    from django.contrib.sites.models import Site
    site = Site.objects.get_current()
    host = request.get_host()
    is_site_subdomain = (
        host == site.domain
        or host.endswith('.' + site.domain)
    )
    return not is_site_subdomain
