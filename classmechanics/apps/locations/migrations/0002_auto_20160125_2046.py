# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='description_markup_type',
            field=models.CharField(default='markdown', choices=[('', '--'), ('markdown', 'markdown')], max_length=30),
        ),
    ]
