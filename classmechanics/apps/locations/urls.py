from django.conf.urls import url

from .views import LocationDetailView, LocationRedirectView


urlpatterns = [
    url(
        r'^(?P<pk>\d+)/$',
        LocationRedirectView.as_view(),
    ),
    url(
        r'^(?P<pk>\d+)-(?P<slug>[\w-]+)/$',
        LocationDetailView.as_view(),
        name='detail',
    ),
]
