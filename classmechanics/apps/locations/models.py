from django.db import models
from django.utils.text import slugify
from django_extensions.db.models import TimeStampedModel
from markupfield.fields import MarkupField
from subdomains.utils import reverse


class Location(TimeStampedModel):
    """A location where classes and/or camps are held."""

    name = models.CharField(max_length=200)
    organization = models.ForeignKey('organizations.Organization')
    description = MarkupField(default_markup_type='markdown')

    def __str__(self):
        return '{0.name} ({0.organization.name})'.format(self)

    def get_detail_url(self, brand=None, subdomain=None):
        return reverse(
            'locations:detail',
            subdomain=(subdomain if brand is None else brand.site_name),
            kwargs=dict(
                pk=self.pk,
                slug=slugify(self.name),
            ),
        )
