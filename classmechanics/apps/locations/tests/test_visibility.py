from urllib.parse import urlparse

from django.contrib.sites.models import Site
from django.test import TestCase
from model_mommy import mommy

from classmechanics.apps.brands.models import Brand
from classmechanics.tests.lib.secureclient import SecureClient
from ..models import Location


class LocationVisibilityTestCase(TestCase):

    def setUp(self):
        self.c = SecureClient()
        self.brand = mommy.make(Brand, site_name='brand')
        site = Site.objects.get_current()
        self.brand_host = '{}.{}'.format(self.brand.site_name, site.domain)
        self.unrelated = mommy.make(Brand, site_name='unrelated')
        self.unrelated_host = '{}.{}'.format(self.unrelated.site_name, site.domain)
        self.location = mommy.make(
            Location,
            organization=self.brand.organization,
            description='',
        )

    def test__related_brand__show_location_detail(self):
        url = self.location.get_detail_url(brand=self.brand)
        parsed = urlparse(url)
        response = self.c.get(parsed.path, HTTP_HOST=parsed.netloc)
        self.assertEqual(200, response.status_code)

    def test__unrelated_brand__404(self):
        url = self.location.get_detail_url(brand=self.unrelated)
        parsed = urlparse(url)
        response = self.c.get(parsed.path, HTTP_HOST=parsed.netloc)
        self.assertEqual(404, response.status_code)
