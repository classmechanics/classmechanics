from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, RedirectView

from .models import Location


class LocationDetailView(DetailView):

    model = Location
    context_object_name = 'location'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(organization=self.request.organization)
        return qs


class LocationRedirectView(RedirectView):

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        location = get_object_or_404(Location, pk=self.kwargs['pk'])
        return location.get_detail_url(brand=self.request.brand)
