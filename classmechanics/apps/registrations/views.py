from django.conf import settings

from allauth.account.forms import LoginForm, SignupForm
from django.contrib.contenttypes.models import ContentType
from django.http import Http404, HttpResponseBadRequest, HttpResponseRedirect
from django.views.generic import TemplateView

from classmechanics.apps.camps.models import Camp
from classmechanics.apps.classes.models import Class
from classmechanics.apps.orders.models import Order
from classmechanics.apps.registrations.forms import (
    CampRegistrationForm, ClassRegistrationForm)
from subdomains.utils import reverse

from classmechanics.apps.families.forms import AgreementForm, \
    ChildFormSet, FamilyForm
from classmechanics.apps.families.models import Family


class InitiateRegistrationView(TemplateView):

    template_name = 'registrations/initiate.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        object = self.get_object()
        if object.brand != self.request.brand:
            raise Http404()
        data['object'] = object
        data['next'] = self.here_url()
        if self.request.user.is_authenticated():
            data.update(self.get_context_data_families())
            if data['families']:
                data.update(self.get_context_data_register())
            else:
                data.update(self.get_context_data_family_profile())
        else:
            data.update(self.get_context_data_anonymous())
        return data

    def get_context_data_anonymous(self):
        return {
            'login_form': self.get_form_login(),
            'signup_form': self.get_form_signup(),
        }

    def get_context_data_families(self):
        families = Family.objects.filter(adult__user=self.request.user)
        return {
            'families': families,
        }

    def get_context_data_family_profile(self):
        return {
            'permission_agreement_form': self.get_form_permission_agreement(),
            'family_form': self.get_form_family(),
            'child_formset': self.get_formset_child(),
        }

    def get_context_data_register(self):
        return {
            'registration_form': self.get_form_registration(),
        }

    def get_form_family(self):
        if self.was_family_profile_post(self.request):
            data = self.request.POST
        else:
            data = None
        return FamilyForm(data)

    def get_form_login(self):
        form = LoginForm()
        del form.fields['login'].widget.attrs['autofocus']
        return form

    def get_form_permission_agreement(self):
        if self.was_family_profile_post(self.request):
            data = self.request.POST
        else:
            data = None
        return AgreementForm(data)

    def get_form_registration(self, instance=None):
        if self.was_registration_post(self.request):
            data = self.request.POST
        else:
            data = None
        obj = self.get_object()
        family = self.request.family
        if isinstance(obj, Class):
            return ClassRegistrationForm(
                data, brand=self.request.brand, family=family, cls=obj,
                instance=instance)
        elif isinstance(obj, Camp):
            return CampRegistrationForm(
                data, brand=self.request.brand, family=family, camp=obj,
                instance=instance)

    def get_form_signup(self):
        return SignupForm()

    def get_formset_child(self, instance=None):
        if self.was_family_profile_post(self.request):
            data = self.request.POST
        else:
            data = None
        return ChildFormSet(data, instance=instance)

    def get_object(self):
        content_type = ContentType.objects.get(id=self.content_type_id)
        object = content_type.get_object_for_this_type(id=self.object_id)
        return object

    def here_url(self):
        return reverse(
            'registrations:initiate',
            subdomain='accounts',
            scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL,
        )

    def order_url(self, order):
        return reverse(
            'orders:detail',
            kwargs=dict(pk=order.id),
            subdomain='accounts',
            scheme=settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL,
        )

    def set_registration_object_from_session(self):
        session = self.request.session
        if 'registration_content_type_id' not in session:
            return HttpResponseBadRequest()
        self.content_type_id = session['registration_content_type_id']
        self.object_id = session['registration_object_id']

    def was_family_profile_post(self, request):
        return request.POST.get('_family_profile') == '1'

    def was_registration_post(self, request):
        return request.POST.get('_registration') == '1'

    def get(self, request, *args, **kwargs):
        # When we GET, there must already be a registration object
        # in the session.
        self.set_registration_object_from_session()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if self.was_family_profile_post(request):
            # On POST of initial family profile, process that form,
            # then redirect back to the registration page.
            self.set_registration_object_from_session()
            context = self.get_context_data(**kwargs)
            permission_agreement_form = context['permission_agreement_form']
            family_form = context['family_form']
            if all([
                permission_agreement_form.is_valid(),
                family_form.is_valid(),
            ]):
                family = family_form.save()
                family.agreement_set.create(brand=self.request.brand)
                family.adult_set.create(user=self.request.user)
                child_formset = self.get_formset_child(instance=family)
                if child_formset.is_valid():
                    child_formset.save()
                    return HttpResponseRedirect(self.here_url())
            else:
                return self.render_to_response(context)
        elif self.was_registration_post(request):
            # On POST of registration form, create registration(s),
            # create an order, then redirect to that order.
            self.set_registration_object_from_session()
            context = self.get_context_data(**kwargs)
            form = context['registration_form']
            if form.is_valid():
                registrations = form.save()
                order = Order.objects.get_or_create_for_brand_and_family(
                    self.request.brand, self.request.family)
                for registration in registrations:
                    registration.add_to_order(order)
                return HttpResponseRedirect(self.order_url(order))
            else:
                return self.render_to_response(context)
        else:
            # On initial POST, set the registration object in the session.
            session = self.request.session
            self.content_type_id = self.request.POST['content_type_id']
            self.object_id = self.request.POST['object_id']
            session['registration_content_type_id'] = self.content_type_id
            session['registration_object_id'] = self.object_id
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)
