# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0006_auto_20160131_0133'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('brands', '0010_auto_20160131_0054'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(verbose_name='created', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(verbose_name='modified', auto_now=True)),
                ('event_object_id', models.IntegerField()),
                ('attendee_object_id', models.IntegerField()),
                ('state', django_fsm.FSMField(default='submitted', max_length=50)),
                ('payment_state', django_fsm.FSMField(default='unpaid', max_length=50)),
                ('attendee_content_type', models.ForeignKey(related_name='+', to='contenttypes.ContentType')),
                ('brand', models.ForeignKey(to='brands.Brand')),
                ('event_content_type', models.ForeignKey(related_name='+', to='contenttypes.ContentType')),
                ('family', models.ForeignKey(to='families.Family')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
    ]
