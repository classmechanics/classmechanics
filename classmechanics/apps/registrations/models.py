from django.contrib.contenttypes.generic import GenericForeignKey
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django_fsm import FSMField, transition


class Registration(TimeStampedModel):
    """

    .. uml:: registrations.Registration.state
        @startuml
        [*] --> submitted
        ordered --> cancelled_by_event : cancel_by_event
        ordered --> cancelled_by_user : cancel_by_user
        ordered --> registered : register
        registered --> attended : record_attendance
        registered --> cancelled_by_event : cancel_by_event
        registered --> cancelled_by_user : cancel_by_user
        registered --> missed : record_absence
        submitted --> ordered : order
        submitted --> waiting : add_to_waiting_list
        waiting --> cancelled_by_event : cancel_by_event
        waiting --> cancelled_by_user : cancel_by_user
        attended --> [*]
        cancelled_by_event --> [*]
        cancelled_by_user --> [*]
        missed --> [*]
        @enduml

    .. uml:: registrations.Registration.payment_state
        @startuml
        [*] --> unpaid
        paid_in_full --> nonrefundable : make_nonrefundable
        paid_in_full --> refunded : note_refund
        paid_in_full --> refunded_with_fee : note_refund_with_fee
        paid_in_part --> paid_in_full : note_full_payment
        paid_in_part --> refunded : note_refund
        unpaid --> paid_in_full : note_full_payment
        unpaid --> paid_in_part : note_partial_payment
        nonrefundable --> [*]
        refunded --> [*]
        refunded_with_fee --> [*]
        @enduml
    """

    brand = models.ForeignKey('brands.Brand')
    family = models.ForeignKey('families.Family')
    event_content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    event_object_id = models.IntegerField()
    event = GenericForeignKey('event_content_type', 'event_object_id')
    attendee_content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    attendee_object_id = models.IntegerField()
    attendee = GenericForeignKey('attendee_content_type', 'attendee_object_id')
    state = FSMField(default='submitted')
    payment_state = FSMField(default='unpaid')

    @property
    def price(self):
        return self.event.price

    def __str__(self):
        return '{} @ {}'.format(self.attendee, self.event)

    # state

    @transition(field=state, source='submitted', target='waiting')
    def add_to_waiting_list(self):
        pass

    @transition(field=state, source='submitted', target='ordered')
    def add_to_order(self, order):
        order.item_set.create(object=self)

    @transition(field=state, source='ordered', target='registered')
    def register(self):
        pass

    @transition(field=state, source=['ordered', 'waiting', 'registered'], target='cancelled_by_user')
    def cancel_by_user(self):
        pass

    @transition(field=state, source=['ordered', 'waiting', 'registered'], target='cancelled_by_event')
    def cancel_by_event(self):
        pass

    @transition(field=state, source='registered', target='attended')
    def record_attendance(self):
        pass

    @transition(field=state, source='registered', target='missed')
    def record_absence(self):
        pass

    # payment_state

    @transition(field=payment_state, source='unpaid', target='paid_in_part')
    def note_partial_payment(self):
        pass

    @transition(field=payment_state, source=['paid_in_part', 'unpaid'], target='paid_in_full')
    def note_full_payment(self):
        pass

    @transition(field=payment_state, source='paid_in_full', target='nonrefundable')
    def make_nonrefundable(self):
        pass

    @transition(field=payment_state, source=['paid_in_part', 'paid_in_full'], target='refunded')
    def note_refund(self):
        pass

    @transition(field=payment_state, source='paid_in_full', target='refunded_with_fee')
    def note_refund_with_fee(self):
        pass
