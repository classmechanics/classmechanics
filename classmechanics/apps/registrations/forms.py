from functools import partial, wraps

from django import forms
from django.contrib.contenttypes.models import ContentType

from classmechanics.apps.classes.models import Session
from classmechanics.apps.families.models import Family, Child
from .models import Registration


class EncodesCleanedDataToContentType(object):

    def encode_cleaned_data_item(self, key):
        data = self.cleaned_data
        obj = data[key]
        content_type_name = '{}_content_type'.format(key)
        object_id_name = '{}_object_id'.format(key)
        data[content_type_name] = ContentType.objects.get_for_model(obj)
        data[object_id_name] = obj.id


class CampRegistrationForm(forms.ModelForm, EncodesCleanedDataToContentType):

    attendees = forms.ModelMultipleChoiceField(
        Child,
        label='Children attending',
        required=True,
        widget=forms.CheckboxSelectMultiple,
    )

    class Meta:
        model = Registration
        fields = ['attendees']

    def __init__(self, *args, **kwargs):
        self._brand = kwargs.pop('brand')
        self._camp = kwargs.pop('camp')
        self._family = kwargs.pop('family')
        super(CampRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['attendees'].queryset = self._family.child_set.all()

    def save(self, commit=True):
        self.cleaned_data['brand'] = self._brand
        self.cleaned_data['event'] = self._camp
        self.cleaned_data['family'] = self._family
        self.encode_cleaned_data_item('event')
        attendees = self.cleaned_data.pop('attendees')
        registrations = []
        for attendee in attendees:
            self.cleaned_data['attendee'] = attendee
            self.encode_cleaned_data_item('attendee')
            registration = Registration.objects.create(**self.cleaned_data)
            registrations.append(registration)
        return registrations


class ClassRegistrationForm(forms.ModelForm, EncodesCleanedDataToContentType):

    session = forms.ModelChoiceField(
        Session,
        label='Which session will you be going to?',
        required=True,
        widget=forms.RadioSelect,
        empty_label=None,
    )
    attendees = forms.ModelMultipleChoiceField(
        Child,
        label='Who will be attending?',
        required=True,
        widget=forms.CheckboxSelectMultiple,
    )

    class Meta:
        model = Registration
        fields = ['session', 'attendees']

    def __init__(self, *args, **kwargs):
        self._brand = kwargs.pop('brand')
        self._family = kwargs.pop('family')
        cls = kwargs.pop('cls')
        super(ClassRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['session'].queryset = cls.session_set.all()
        self.fields['attendees'].queryset = self._family.child_set.all()

    def save(self, commit=True):
        self.cleaned_data['brand'] = self._brand
        self.cleaned_data['event'] = self.cleaned_data.pop('session')
        self.cleaned_data['family'] = self._family
        self.encode_cleaned_data_item('event')
        attendees = self.cleaned_data.pop('attendees')
        registrations = []
        for attendee in attendees:
            self.cleaned_data['attendee'] = attendee
            self.encode_cleaned_data_item('attendee')
            registration = Registration.objects.create(**self.cleaned_data)
            registrations.append(registration)
        return registrations
