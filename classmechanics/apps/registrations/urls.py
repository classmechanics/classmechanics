from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from .views import InitiateRegistrationView

urlpatterns = [
    url(
        r'^initiate/$',
        csrf_exempt(InitiateRegistrationView.as_view()),
        name='initiate',
    ),
]
