from urllib.parse import urlparse

from behave import *
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.text import slugify
from model_mommy import mommy
import parse

from classmechanics.apps.families.models import Family, Child, Adult
from subdomains.utils import reverse as sdreverse

from classmechanics.apps.brands.models import Brand
from classmechanics.apps.classes.models import Class
from classmechanics.tests.lib.bddlibrary import doc, submit_form


@parse.with_pattern(r'[\w\s]+')
def parse_name(text):
    return text

register_type(Name=parse_name)


# -- ( GIVEN ) ---------------------------------------------------------------

@given('there is a brand "{brand_name:Name}"')
def impl(context, brand_name):
    context.brand = mommy.make(
        Brand, name=brand_name, site_name=slugify(brand_name))

@given('that brand offers a "{class_name:Name}" class')
def impl(context, class_name):
    context.cls = mommy.make(Class, name=class_name, brand=context.brand)

@given('I will visit their website')
def impl(context):
    context.subdomain = context.brand.site_name

@given('I am anonymous')
def impl(context):
    pass

@given('I have a family profile with these children')
def impl(context):
    family = mommy.make(Family)
    mommy.make(Adult, user=context.user, family=family)
    for row in context.table:
        mommy.make(Child, name=row['name'], family=family)

@given('I have an account')
def impl(context):
    context.user = mommy.make(User, email='skippyjon@jon.es')
    context.user.set_password('password')
    context.user.save()
    assert context.client.login(email=context.user.email, password='password')


# -- ( WHEN ) ----------------------------------------------------------------

@when('I visit the "{class_name:Name}" class detail page')
def impl(context, class_name):
    url = urlparse(sdreverse(
        'classes:detail',
        kwargs=dict(
            pk=context.cls.pk,
            slug=slugify(context.cls),
        ),
        subdomain=context.subdomain,
    ))
    site = Site.objects.get_current()
    host = '{}.{}'.format(context.brand.site_name, site.domain)
    context.response = context.client.get(
        url.path, secure=True, HTTP_HOST=host)
    assert context.response.status_code == 200

@when('I click the Register button')
def impl(context):
    d = doc(context)
    register = d.find('.btn-register-now')
    context.response = submit_form(context, register)


# -- ( THEN ) ----------------------------------------------------------------

@then('I see login options')
def impl(context):
    d = doc(context)
    assert d.find('#initiate-anonymous')

@then('I am asked to complete a family profile')
def impl(context):
    d = doc(context)
    assert d.find('#initiate-family-profile')

@then('I see a class registration form')
def impl(context):
    d = doc(context)
    assert d.find('#initiate-registration')
