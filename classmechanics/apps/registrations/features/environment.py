from django.db import transaction
from django.test import Client


def before_scenario(context, scenario):
    context.client = Client()
    transaction.set_autocommit(False)


def after_scenario(context, scenario):
    del context.client
    transaction.rollback()
