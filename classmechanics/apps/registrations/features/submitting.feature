Feature: submitting a registration

  Background:
    Given there is a brand "PPSS"
    And that brand offers a "Hexaflexigons" class
    And I will visit their website
    And I have an account
    And I have a family profile with these children
      | name  |
      | Alice |
      | Bob   |
    And I visit the "Hexaflexigons" class detail page
    And I click the Register button
