Feature: initiating a registration

  Background:
    Given there is a brand "PPSS"
    And that brand offers a "Hexaflexigons" class
    And I will visit their website

  Scenario: anonymous users get a login form
    Given I am anonymous
    When I visit the "Hexaflexigons" class detail page
    And I click the Register button
    Then I see login options

  Scenario: newly registered users get a family profile form
    Given I have an account
    When I visit the "Hexaflexigons" class detail page
    And I click the Register button
    Then I am asked to complete a family profile

  Scenario: users with a family profile get the registration form
    Given I have an account
    And I have a family profile with these children
      | name  |
      | Alice |
      | Bob   |
    When I visit the "Hexaflexigons" class detail page
    And I click the Register button
    Then I see a class registration form
