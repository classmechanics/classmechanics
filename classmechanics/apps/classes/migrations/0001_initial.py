# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields
import markupfield.fields
from decimal import Decimal
import djmoney.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_auto_20160125_2046'),
        ('brands', '0002_brand_default_timezone'),
        ('instructors', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('name', models.CharField(max_length=200)),
                ('date', models.DateField()),
                ('price_currency', djmoney.models.fields.CurrencyField(default='USD', max_length=3, editable=False, choices=[('USD', 'US Dollar')])),
                ('price', djmoney.models.fields.MoneyField(default=Decimal('0.0'), decimal_places=2, max_digits=8, default_currency='USD')),
                ('punches', models.PositiveIntegerField(default=1)),
                ('description', markupfield.fields.MarkupField(rendered_field=True)),
                ('description_markup_type', models.CharField(default='markdown', max_length=30, choices=[('', '--'), ('markdown', 'markdown')])),
                ('_description_rendered', models.TextField(editable=False)),
                ('published', models.BooleanField()),
                ('brand', models.ForeignKey(to='brands.Brand')),
                ('instructors', models.ManyToManyField(to='instructors.Instructor')),
                ('location', models.ForeignKey(to='locations.Location')),
            ],
            options={
                'verbose_name_plural': 'Classes',
            },
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('min_age', models.PositiveIntegerField()),
                ('max_age', models.PositiveIntegerField(blank=True, null=True)),
                ('cls', models.ForeignKey(verbose_name='Class', to='classes.Class')),
            ],
            options={
                'ordering': ('start_time',),
            },
        ),
    ]
