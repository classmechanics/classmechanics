from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from .models import Appearance, Class, Session


class AppearanceInline(admin.StackedInline):

    model = Appearance
    fields = ['image']
    readonly_fields = ['modified']


class SessionInline(admin.TabularInline):

    fields = ['start_time', 'end_time', 'min_age', 'max_age']
    readonly_fields = ['modified']
    model = Session
    extra = 2


class ClassAdmin(admin.ModelAdmin):

    def brand_link(self, instance):
        if instance.id is not None:
            brand = instance.brand
            url = reverse(
                'admin:%s_%s_change' % (brand._meta.app_label,
                                        brand._meta.model_name),
                args=(brand.id,))
            return format_html('<a href="{}">{}</a>', url, brand.site_name)

    list_display = ['name', 'date', 'brand_link', 'modified']
    readonly_fields = ['brand_link', 'created', 'modified']
    ordering = ('-date',)
    inlines = [SessionInline, AppearanceInline]

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Class, ClassAdmin)
