import cloudinary.models
from django.db import models
from django.utils.text import slugify
from django_extensions.db.models import TimeStampedModel
from djmoney.models.fields import MoneyField
from markupfield.fields import MarkupField
from subdomains.utils import reverse


class Appearance(TimeStampedModel):
    """The visual identity of a brand."""

    cls = models.OneToOneField('Class')
    image = cloudinary.models.CloudinaryField('image', blank=True, null=True)

    def __str__(self):
        return 'Appearance for {}'.format(self.cls.name)


class Class(TimeStampedModel):
    """A class with 1+ sessions, on a single day, at a single location."""

    brand = models.ForeignKey('brands.Brand')
    name = models.CharField(max_length=200)
    date = models.DateField()
    price = MoneyField(max_digits=8, decimal_places=2, default_currency='USD')
    punches = models.PositiveIntegerField(default=1, blank=True, null=True)
    description = MarkupField(default_markup_type='markdown')
    instructors = models.ManyToManyField('instructors.Instructor')
    location = models.ForeignKey('locations.Location')
    published = models.BooleanField()

    class Meta:
        ordering = ('date',)
        verbose_name_plural = 'Classes'

    def __str__(self):
        return self.name

    def get_detail_url(self, brand=None, subdomain=None):
        return reverse(
            'classes:detail',
            subdomain=(subdomain if brand is None else brand.site_name),
            kwargs=dict(
                pk=self.pk,
                slug=slugify(self.name),
            ),
        )


class Session(TimeStampedModel):
    """A class session with a start/end time."""

    cls = models.ForeignKey('Class', verbose_name='Class')
    start_time = models.TimeField()
    end_time = models.TimeField()
    min_age = models.PositiveIntegerField()
    max_age = models.PositiveIntegerField(blank=True, null=True)

    @property
    def price(self):
        return self.cls.price

    class Meta:
        ordering = ('start_time',)

    def __str__(self):
        return '{0.cls.name} @ {0.start_time}'.format(self)
