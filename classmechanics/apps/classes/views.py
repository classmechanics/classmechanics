from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, RedirectView

from .models import Class


class ClassDetailView(DetailView):

    model = Class
    context_object_name = 'cls'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(brand__organization=self.request.organization)
        return qs


class ClassRedirectView(RedirectView):

    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        cls = get_object_or_404(Class, pk=self.kwargs['pk'])
        return cls.get_detail_url(brand=self.request.brand)
