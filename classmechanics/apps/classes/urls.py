from django.conf.urls import url

from .views import ClassDetailView, ClassRedirectView


urlpatterns = [
    url(
        r'^(?P<pk>\d+)/$',
        ClassRedirectView.as_view(),
    ),
    url(
        r'^(?P<pk>\d+)-(?P<slug>[\w-]+)/$',
        ClassDetailView.as_view(),
        name='detail',
    ),
]
