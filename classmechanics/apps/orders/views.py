from django.core.urlresolvers import reverse
from django.views.generic import DetailView, RedirectView

from .models import Order


class CartRedirect(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        order = Order.objects.get_or_create_for_brand_and_family(
            self.request.brand, self.request.family)
        return reverse('orders:detail', kwargs=dict(pk=order.pk))


class OrderDetail(DetailView):

    model = Order
