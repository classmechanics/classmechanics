from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django_fsm import FSMField, transition


class OrderManager(models.Manager):

    def get_or_create_for_brand_and_family(self, brand, family):
        order = self.filter(brand=brand, family=family, state='preparing').first()
        if order is None:
            order = self.create(brand=brand, family=family)
        return order


class Order(TimeStampedModel):
    """
    .. uml:: orders.Order.state
        @startuml
        [*] --> preparing
        checking_out --> cancelled_by_user : cancel_by_user
        checking_out --> invoiced : create_invoice
        invoiced --> cancelled_by_user : cancel_by_user
        invoiced --> completed : complete
        preparing --> cancelled_by_user : cancel_by_user
        preparing --> checking_out : checkout
        cancelled_by_user --> [*]
        completed --> [*]
        @enduml
    """

    objects = OrderManager()

    brand = models.ForeignKey('brands.Brand')
    family = models.ForeignKey('families.Family')
    state = FSMField(default='preparing')

    @property
    def subtotal(self):
        return sum(item.object.price for item in self.item_set.all())

    @transition(state, source='preparing', target='checking_out')
    def checkout(self):
        pass

    @transition(state, source='checking_out', target='invoiced')
    def create_invoice(self):
        pass

    @transition(state, source=['preparing', 'checking_out', 'invoiced'],
                target='cancelled_by_user')
    def cancel_by_user(self):
        pass

    @transition(state, source='invoiced', target='completed')
    def complete(self):
        pass


class Item(TimeStampedModel):
    """
    """

    order = models.ForeignKey('Order')
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    object_id = models.IntegerField()
    object = GenericForeignKey()
