from django.contrib import admin

from classmechanics.apps.registrations.models import Registration
from .models import Order, Item


class ItemInline(admin.TabularInline):

    model = Item
    fields = ['description']
    readonly_fields = ['description']
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False

    def description(self, item):
        return str(item.object)


class OrderAdmin(admin.ModelAdmin):

    list_display = ['brand', 'family', 'state']
    inlines = [ItemInline]

    class Media:
        css = {"all": ("css/hide_admin_original.css",)}

admin.site.register(Order, OrderAdmin)
