from django import template

from ..models import Order


register = template.Library()


@register.filter()
def cart_for_family(brand, family):
    return Order.objects.get_or_create_for_brand_and_family(brand, family)
