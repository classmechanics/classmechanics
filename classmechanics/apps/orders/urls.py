from django.conf.urls import url


from .views import CartRedirect, OrderDetail


urlpatterns = [
    url(r'^(?P<pk>\d+)/$', OrderDetail.as_view(), name='detail'),
    url(r'^cart/$', CartRedirect.as_view(), name='cart'),
]
