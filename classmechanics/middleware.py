from django.conf import settings
from django.contrib.sites.models import Site
from django.middleware.csrf import CsrfViewMiddleware
from django.utils.cache import patch_vary_headers


class SubdomainAwareCsrfViewMiddleware(CsrfViewMiddleware):

    def process_response(self, request, response):
        if getattr(response, 'csrf_processing_done', False):
            return response

        # If CSRF_COOKIE is unset, then CsrfViewMiddleware.process_view was
        # never called, probably because a request middleware returned a response
        # (for example, contrib.auth redirecting to a login page).
        if request.META.get("CSRF_COOKIE") is None:
            return response

        if not request.META.get("CSRF_COOKIE_USED", False):
            return response

        # --> set the cookie domain based on the request <--
        csrf_cookie_domain = request.META['HTTP_HOST']
        site = Site.objects.get_current()
        if csrf_cookie_domain.startswith('www.'):
            csrf_cookie_domain = '.{}'.format(csrf_cookie_domain[4:])
        elif csrf_cookie_domain.endswith('.{}'.format(site.domain)):
            csrf_cookie_domain = '.{}'.format(site.domain)
        if ':' in csrf_cookie_domain:
            csrf_cookie_domain = csrf_cookie_domain.split(':', 1)[0]

        # Set the CSRF cookie even if it's already set, so we renew
        # the expiry timer.
        response.set_cookie(settings.CSRF_COOKIE_NAME,
                            request.META["CSRF_COOKIE"],
                            max_age=settings.CSRF_COOKIE_AGE,
                            # --> set the cookie domain based on the request <--
                            domain=csrf_cookie_domain,
                            path=settings.CSRF_COOKIE_PATH,
                            secure=settings.CSRF_COOKIE_SECURE,
                            httponly=settings.CSRF_COOKIE_HTTPONLY
                            )
        # Content varies with the CSRF cookie, so set the Vary header.
        patch_vary_headers(response, ('Cookie',))
        response.csrf_processing_done = True
        return response
