from urllib.parse import urlparse

from django.contrib.sites.models import Site
from pyquery import PyQuery as PQ


def doc(context):
    """
    A PyQuery document from the context's response.

    :param context: Context to get .response from
    :return: PyQuery doc of the response
    """
    return PQ(context.response.content)


def submit_form(context, element):
    """
    Submitting the form containing an element and return the response.

    :param context: Test context w/ .client (Django test client)
    :param element: The PyQuery element inside the form to submit.
    :return: Django response object.
    """
    form = element.parent('form')
    #
    action = form.attr('action') or None
    if not action:
        raise ValueError('Empty action')
    action_url = urlparse(action)
    site = Site.objects.get_current()
    default_host = '{}.{}'.format(context.subdomain, site.domain)
    action_host = action_url.netloc or default_host
    is_subdomain = action_host.endswith('.{}'.format(site.domain))
    action_path = action_url.path
    #
    method = (form.attr('method') or 'get').lower()
    if method == 'post':
        method = context.client.post
    elif method == 'get':
        method = context.client.get
    else:
        raise ValueError('Unknown method {!r}'.format(method))
    #
    data = dict(
        form.find('input').map(lambda i, e: (
            PQ(e).attr('name'),
            PQ(e).val(),
        ))
    )
    # TODO: place select values and textareas into values dict
    #
    return method(path=action_path, data=data, secure=is_subdomain,
                  HTTP_HOST=action_host)
