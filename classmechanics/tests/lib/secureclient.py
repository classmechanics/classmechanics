from django.test import Client
from django.test.client import MULTIPART_CONTENT


class SecureClient(Client):

    def get(self, path, data=None, follow=False, secure=True, **extra):
        return super().get(path, data, follow, secure=True, **extra)

    def post(self, path, data=None, content_type=MULTIPART_CONTENT, follow=False, secure=True, **extra):
        return super().post(path, data, content_type, follow, secure, **extra)

    def head(self, path, data=None, follow=False, secure=True, **extra):
        return super().head(path, data, follow, secure, **extra)

    def trace(self, path, data='', follow=False, secure=True, **extra):
        return super().trace(path, data, follow, secure, **extra)

    def options(self, path, data='', content_type='application/octet-stream', follow=False, secure=True, **extra):
        return super().options(path, data, content_type, follow, secure, **extra)

    def put(self, path, data='', content_type='application/octet-stream', follow=False, secure=True, **extra):
        return super().put(path, data, content_type, follow, secure, **extra)

    def patch(self, path, data='', content_type='application/octet-stream', follow=False, secure=True, **extra):
        return super().patch(path, data, content_type, follow, secure, **extra)

    def delete(self, path, data='', content_type='application/octet-stream', follow=False, secure=True, **extra):
        return super().delete(path, data, content_type, follow, secure, **extra)

    def generic(self, method, path, data='', content_type='application/octet-stream', secure=True, **extra):
        return super().generic(method, path, data, content_type, secure, **extra)
