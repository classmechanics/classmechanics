from django.contrib.auth.models import User
from django.test import Client, TestCase


class RedirectsTestCase(TestCase):

    def setUp(self):
        self.c = Client()

    def test__http__any_subdomain__redirect_to_https(self):
        response = self.c.get('/', secure=False, HTTP_HOST='example.com')
        self.assertEqual('https://example.com:443/', response.url)
        response = self.c.get('/', secure=False, HTTP_HOST='xyz.example.com')
        self.assertEqual('https://xyz.example.com:443/', response.url)

    def test__https__base_domain__redirect_to_www_prefixed(self):
        response = self.c.get('/', secure=True, HTTP_HOST='example.com')
        self.assertEqual('https://www.example.com/', response.url)

    def test__https__any_subdomain__no_redirect_to_www_prefixed(self):
        response = self.c.get('/', secure=True, HTTP_HOST='xyz.example.com')
        self.assertEqual(404, response.status_code)

    def test__https__any_subdomain__login_or_logout__redirect_to_accounts(self):
        response = self.c.get('/login/', secure=True, HTTP_HOST='www.example.com')
        self.assertEqual('https://accounts.example.com/login/', response.url)
        response = self.c.get('/logout/', secure=True, HTTP_HOST='www.example.com')
        self.assertEqual('https://accounts.example.com/logout/', response.url)

    def test__https__any_subdomain__login_or_logout__redirect_keeps_query_params(self):
        response = self.c.get('/login/?next=//www.example.com/', secure=True, HTTP_HOST='www.example.com')
        self.assertEqual('https://accounts.example.com/login/?next=//www.example.com/', response.url)

    def test__https__accounts__root__redirect_to_profile(self):
        response = self.c.get('/', secure=True, HTTP_HOST='accounts.example.com')
        self.assertEqual('https://accounts.example.com/profile/', response.url)

    def test__https__accounts__login_success__redirect_across_subdomains(self):
        u = User.objects.create(username='user', email='e@ma.il')
        u.set_password('password')
        u.save()
        login_params = {'login': 'e@ma.il', 'password': 'password'}
        response = self.c.post('/login/?next=//www.example.com/', login_params,
                               secure=True, HTTP_HOST='accounts.example.com')
        self.assertEqual('https://www.example.com/', response.url)

    def test__https__accounts__login_success__no_redirect_to_external_domain(
        self):
        u = User.objects.create(username='user', email='e@ma.il')
        u.set_password('password')
        u.save()
        login_params = {'login': 'e@ma.il', 'password': 'password'}
        response = self.c.post('/login/?next=//www.other.com/', login_params,
                               secure=True, HTTP_HOST='accounts.example.com')
        self.assertEqual('https://accounts.example.com/profile/', response.url)
