-r development.txt

# HTTPS enforcement
django-sslify==0.2.7

# BDD
-e git+https://github.com/django-behave/django-behave@2e26994c78bbfe9e966cffc0338b2dd88507f7a7#egg=django-behave
