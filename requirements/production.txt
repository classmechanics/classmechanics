# Whatever package needed in production(if any) goes there.

-r base.txt

# Static & Media
whitenoise==2.0.6

# HTTPS enforcement
django-sslify==0.2.7
