#!/usr/bin/env bash
#
# Wrapper around running Fabric from the correct directory
# and with the necessary Python version.

cd $(dirname $0)

if [ ! -d .fabenv ]; then
    virtualenv -p python2.7 .fabenv
fi

if [ ! -f .fabenv/bin/fab ]; then
    .fabenv/bin/pip install -U pip
    .fabenv/bin/pip install -U fabric requests
fi

.fabenv/bin/fab $@
