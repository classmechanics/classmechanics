#!/usr/bin/env bash
#
# Rebuilds the django container image, then restarts it and tails the logs.

docker-compose stop -t 3 django && \
docker-compose up --force-recreate -d && \
docker-compose logs
