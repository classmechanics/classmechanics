FROM python:3.5

ENV PYTHONUNBUFFERED 1

RUN mkdir /requirements
COPY requirements/base.txt /requirements
RUN pip install -r /requirements/base.txt
COPY requirements/development.txt /requirements
RUN pip install -r /requirements/development.txt
COPY requirements/testing.txt /requirements
RUN pip install -r /requirements/testing.txt
