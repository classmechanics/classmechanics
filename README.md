# Class Mechanic #

## Quickstart ##

First create and activate your virtualenv, you can use 
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/).

`cd` to your project and install the dependences into your local environment:

    pip install -r requirements/development.txt

Now build the Docker dev environment, bring it up, and watch its logs:

    docker-compose up -d; docker-compose logs

Initialize the database:

    ./docker-manage.sh migrate

Once everything it's setup you can visit the development server: 
[http://docker.dev:8000/](http://docker.dev:8000/)


## App structure

The following describes the purpose of each app within this project.

It is not in order of implementation, but serves as a general roadmap of where the project intends to go.

It does not take into account visual design or fancy admin UI, which will be an ongoing process of testing and refinement once the system's foundation is in place.

### accounts
- [x] Personal account creation, login, logout.
- [x] Family profile creation.
- [ ] Family dashboard.
- [ ] Family profile editor.
- [ ] Connecting accounts with Facebook and Stripe.

### analytics
- [ ] Assignment of analytics service details to a brand.
- [ ] Transmission of analytics events to services.

### brands
- [x] Details about brand, e.g. name, colors, etc.
- [x] Ownership by organizations.
- [x] Interface between organizations and brands (individual websites).
- [x] CNAME aliases for brands (e.g. mybrandname.com in the URL)

### camps
- [x] Camp list and detail views.
- [ ] Camp attendance/check-in interface.
- [ ] Trigger email campaigns at certain stages.
- [ ] Other features for camp operation TBD

### classes
- [x] Class list and detail views.
- [ ] Class attendance/check-in interface.
- [ ] Trigger email campaigns at certain stages.

### coupons
- [ ] Coupons, w/ info on the discount they provide, expiration date, limited vs unlimited use.
- [ ] Record of applying coupons to orders.
- [ ] Application of discounts to order after applying coupon.

### emails
- [ ] Common transational email campaign templates that all orgs use.
- [ ] Customization of campaign templates.
- [ ] Transform plain email content into org-branded email body.
- [ ] Record emails that go out, and their delivery/open status.
- [ ] Associate them with related objects (e.g. user accounts, families, classes, etc.)

### families
- [x] On-boarding of family profiles.
- [x] Information about children.
- [ ] Information about account membership for adults.
- [ ] Invitations to join families as adults.

### forums
- [ ] Class participant discussions.
- [ ] Private staff/instructor discussions.
- [ ] Photo attachment.
- [ ] Moderation controls.

### instructors
- [ ] Profiles for instructors, including photo, and list of classes and camps taught by instructor.
- [ ] Assignment of instructors to classes.
- [ ] Display of instructor profile in class detail.
- [ ] Association between instructors and user accounts.
- [ ] Instructor dashboard
- [ ] Instructor editing of additional content for emails
- [ ] Information about payment arrangements with an organization.
- [ ] Invoices w/ status for payment arrangement with an organization.
- [ ] Payments via cash/check.
- [ ] Payments via ACH deposit.

### locations
- [x] Information about locations.
- [x] Associations with classes and camps.
- [ ] List of classes and camps available at a location.

### marketing
- [ ] Marketing site for Class Mechanics itself.

### orders
- [x] Orders (including shopping cart) and their status.
- [x] Line-item details of an order, and links to related objects.
- [ ] Order checkout (invoicing)
- [ ] Order payments (w/ link to detail)
- [ ] Offline payment detail (check, cash, money order)
- [ ] Order refunds (w/ link to detail)
- [ ] Offline refund detail (check, cash, money order)
- [ ] Trigger registration changes upon paid-in-full.

### organizations
- [x] Info about organizations.
- [x] Ownership of organizations by users.
- [ ] Organization dashboard
- [ ] Organization reports (of various kinds)

### pages
- [x] Markdown formatted content for brands.
- [x] Tags for including class list.
- [x] Tags for including camp list.
- [ ] Tags and models for attached media.

### stripepayments
- [x] Link between organizations and stripe via preferred user.
- [ ] Stripe payment detail for orders.
- [ ] Stripe refund detail for orders.

### punchcards
- [ ] Types of punchcards that can be purchased.
- [ ] Record of purchased punchcards.
- [ ] Record of punchcard use.
- [ ] Application of punchcard to shopping card and checkout.
- [ ] Upsell and conversion of punchcard in shopping cart and checkout.
- [ ] Image of punchcard w/ punch overlay.
 
### registrations
- [x] Class registration and status.
- [ ] Trigger appropriate emails on registration status changes.
- [ ] Waiting list management.
