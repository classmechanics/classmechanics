#!/bin/bash
set -e
# This entrypoint is used to play nicely with the current cookiecutter configuration.
# Since docker-compose relies heavily on environment variables itself for configuration, we'd have to define multiple
# environment variables just to support cookiecutter out of the box. That makes no sense, so this little entrypoint
# does all this for us.
export REDIS_URL=redis://redis:6379/0

# the official postgres image uses 'postgres' as default user if not set explictly.
if [ -z "$POSTGRES_ENV_POSTGRES_USER" ]; then
    export POSTGRES_ENV_POSTGRES_USER=postgres
fi

export DATABASE_URL=postgres://$POSTGRES_ENV_POSTGRES_USER:$POSTGRES_ENV_POSTGRES_PASSWORD@postgres:5432/$POSTGRES_ENV_POSTGRES_USER

source ~/.nvm/nvm.sh
nvm use v5.4.1

pip install --exists-action i -r requirements/testing.txt
python setup.py develop

# Install third-party packages that we have checked out.
(
    cd /src
    export VENDOR_DIRS=$(find . -type d -maxdepth 1 -not -name '.*' -not -name 'classmechanics')
    pip install -e ${VENDOR_DIRS}
)

exec "$@"
