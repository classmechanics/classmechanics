#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='classmechanics',
    version='1',
    description='',
    long_description='',
    author='Elevencraft Inc.',
    author_email='matt+classmechanics@11craft.com',
    url='https://gitlab.com/classmechanics/classmechanics/',
    packages=[
        'classmechanics',
    ],
    include_package_data=True,
    install_requires=[
    ],
    license="GNU AGPL v3",
    zip_safe=False,
)
